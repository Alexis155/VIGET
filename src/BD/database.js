const sql = require('mssql');
const { database, databaseEOC, databaseKapok } = require('./keys');

const obtenerPool = () => {
	return new Promise((resolve, reject) => {
		sql.on('error', (err) => (err ? console.log('Error de connecion', err) : console.log('Todo bien BD')));
		sql.connect(database)
			.then((pool) => {
				console.log('BD conectada');
				resolve(pool);
			})
			.catch((err) => reject(err));
	});
};

const cerrarPool = (pool) => {
	sql.close();
	console.log('Conexión cerrada');
};

const connect = () => {
	return new Promise((resolve, reject) => {
		sql.on('error', (e) => (e ? console.log('Error de conexión', e) : console.log('Todo bien BD')));
		sql.connect(databaseEOC)
			.then((pool) => {
				console.log('BD conectada');
				resolve(pool);
			})
			.catch((e) => reject(e));
	});
};

const PoolKapok = () => {
	return new Promise((resolve, reject) => {
		sql.on('error', (err) => (err ? console.log('Error de conexión', err) : console.log('Todo bien')));
		sql.connect(databaseKapok)
			.then((pool) => {
				console.log('conectado');
				resolve(pool);
			})
			.catch((err) => reject(err));
	});
};

const cerrarKapok = () => {
	sql.close();
	console.log('Close connection');
};
module.exports = { obtenerPool, connect, cerrarPool, PoolKapok, cerrarKapok };
