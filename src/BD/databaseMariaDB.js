const mariadb = require('mariadb')
const pool = mariadb.createPool({
	user: 'traccar',
	password: '7r9cc9r',
	host: '172.23.176.40',
	port: '3306',
	database: 'traccar',
	connectionLimit: '20',
	timezone: 'utc',
})

const obtenerConexion = () => {
	return new Promise((resolve, reject) => {
		pool.getConnection()
			.then((conn) => {
				resolve(conn)
			})
			.catch((err) => {
				reject(err)
			})
	})
}

module.exports = { obtenerConexion }
