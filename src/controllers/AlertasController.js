var alertasController = {};
const DBalertas = require('../models/VigetAlertas');

alertasController.index = async function (req, res) {
	res.render('VigetAlertas.html');
};
alertasController.count = async function (req, res) {
	try {
		let count = await DBalertas.count();
		res.status(200).json({ alertas: count.recordset });
	} catch (error) {
		console.log('Error:', error);
	}
};

module.exports = alertasController;
