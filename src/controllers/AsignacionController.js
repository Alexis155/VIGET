var AsignacionController = {};
var DBvehiculos = require('../models/Vehiculos');
var DBempleados = require('../models/Empleados');

AsignacionController.index = async function (req, res) {
	try {
		let CveVehi = req.params.CveVehi;
		let datosUnidad = await DBvehiculos.VvigetUnidades(CveVehi);
		let personal = await DBempleados.findAll();
		let unidadRCS = datosUnidad.recordset;
		res.render('asignacion.html', {
			datos: { unidad: unidadRCS[0], personal: personal.recordset },
		});
	} catch (error) {
		console.log('Error:', error);
	}
};
AsignacionController.Insert = async function (req, res) {
	try {
		let cveNumEmp = parseInt(req.body.cveEmp);
		let cveVehiBody = req.body.cveVehi;
		let CveAsigna = req.body.CveAsigna;
		let mensaje = '';
		let estatusBusqueda = '';
		if (cveNumEmp > 0) {
			let asignacionEmpleado = await DBempleados.findId(cveNumEmp);
			if (asignacionEmpleado.recordset.length > 0) {
				if (asignacionEmpleado.recordset[0].CveVehi == cveVehiBody) {
					mensaje =
						'Oficial asignado con estatus abordo en esta unidad ';
					estatusBusqueda = 'ocupado_aqui';
				} else {
					mensaje =
						'Oficial asignado con estatus abordo en unidad ' +
						asignacionEmpleado.recordset[0].CveVehi;
					estatusBusqueda = 'ocupado';
				}
				let consecutivo = asignacionEmpleado.recordset[0].consecutivo;
				let ano = asignacionEmpleado.recordset[0].ano_asignacion;
				res.status(202).json({
					mensaje: mensaje,
					estatus: estatusBusqueda,
					consecutivo,
					ano,
				});
			} else {
				let ultimoRegistro = await DBempleados.findLast();
				let consecutivo = ultimoRegistro.recordset[0].consecutivo + 1;
				let estatusInsert = await DBempleados.insert(
					cveVehiBody,
					cveNumEmp,
					CveAsigna,
					consecutivo
				);
				if (estatusInsert) {
					mensaje = '¡Asignación realizada con éxito!';
					estatusBusqueda = 'libre';
					res.status(202).json({
						mensaje: mensaje,
						estatus: estatusBusqueda,
					});
				} else {
					console.log('Entre aqui');
				}
			}
		}
	} catch (error) {
		console.log('Error:', error);
	}
};

AsignacionController.obtenerAsignaciones = async function (req, res) {
	try {
		let cveVehi = req.body.cveVehi;
		if (cveVehi) {
			let asignaciones = await DBempleados.finAllAsignaciones(cveVehi);
			if (asignaciones.recordset.length > 0) {
				res.status(202).json({ asignaciones: asignaciones.recordset });
			} else {
				res.status(202).json({ asignaciones: 0 });
			}
		}
	} catch (error) {
		console.log('Error', error);
	}
};

AsignacionController.desasginarOficial = async function (req, res) {
	try {
		let mensaje = '';
		let estatusBusqueda = '';
		let consecutivo = parseInt(req.body.consecutivo);
		let anoAsignacion = parseInt(req.body.anoAsignacion);
		let data = await DBempleados.update(consecutivo, anoAsignacion);
		if (data.rowsAffected > 0) {
			mensaje = '¡Des-Asignación realizada con éxito!';
			estatusBusqueda = 'libre';
		} else {
			console.log('No Actualizado');
		}
		res.status(202).json({ mensaje: mensaje, estatus: estatusBusqueda });
	} catch (error) {
		console.log('Error', error);
	}
};

AsignacionController.reAsginarOficial = async function (req, res) {
	try {
		let mensaje = '';
		let estatusBusqueda = '';
		let consecutivo = parseInt(req.body.consecutivo);
		let anoAsignacion = parseInt(req.body.anoAsignacion);
		let cveNumEmp = parseInt(req.body.cveNumEmp);
		let CveAsigna = parseInt(req.body.CveAsigna);
		let cveVehi = req.body.cveVehi;
		let data = await DBempleados.update(consecutivo, anoAsignacion);
		if (data.rowsAffected > 0) {
			let ultimoRegistro = await DBempleados.findLast();
			let consecutivo = ultimoRegistro.recordset[0].consecutivo + 1;
			let estatusInsert = await DBempleados.insert(
				cveVehi,
				cveNumEmp,
				CveAsigna,
				consecutivo
			);
			if (estatusInsert) {
				mensaje = '¡Asignación realizada con éxito!';
				estatusBusqueda = 'libre';
			} else {
				mensaje =
					'¡DesAsignación realizada con éxito! Pero no se puedo realizar la nueva asignación';
				estatusBusqueda = 'libre';
			}
		} else {
			mensaje = '¡Intente de nuevo!';
			estatusBusqueda = 'libre';
		}
		res.status(202).json({ mensaje: mensaje, estatus: estatusBusqueda });
	} catch (error) {
		console.log('Error', error);
	}
};
module.exports = AsignacionController;
