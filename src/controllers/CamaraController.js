var camaraController = {};
const DBcamaras = require('../models/VvigetCamaras');
const DBBotonoes = require('../models/Botones');

camaraController.index = async function (req, res) {
  try {
    var idCam = req.params.idCam;
    var camara = await DBcamaras.findId(idCam);
    camara = camara.recordset
    res.render("video.html",{camara});
  } catch (error) {
    console.log(error);
    res.status(505).json({
      mensaje: "¡Algo salió mal, intentelo de nuevo!",
      estatus: "error",
    });
  }
}
camaraController.botones = async function (req, res) {
  try {
    var idCam = req.params.idCam;
    var camara = await DBBotonoes.findId(idCam);
    camara = camara.recordset
    res.render("videoB.html",{camara});
  } catch (error) {
    console.log(error);
    res.status(505).json({
      mensaje: "¡Algo salió mal, intentelo de nuevo!",
      estatus: "error",
    });
  }
}

module.exports = camaraController