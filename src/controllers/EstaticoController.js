var estaticoController = {};
const DBVehiculos = require('../models/Vehiculos');
const mapaMiddleware = require('../middlewares/mapa');
const DBkapok = require('../models/kapok');

estaticoController.index = async function (req, res) {
	try {
		let fullUrl = req.protocol + '://' + req.get('Host') + req.originalUrl;
		let urlBase = mapaMiddleware.url(fullUrl);
		let key = await DBVehiculos.key(
			urlBase + 'key?username=admin&password=Dst2013admin'
		);
		let unidades = await DBVehiculos.listUnidades(urlBase, key);
		res.render('estatico.html', { unidades: unidades });
	} catch (err) {
		console.log('Error', err);
	}
};

estaticoController.consulta = async function (req, res) {
	try {
		let fullUrl = req.protocol + '://' + req.get('Host') + req.originalUrl;
		let urlBase = mapaMiddleware.url(fullUrl);
		let key = await DBVehiculos.key(
			urlBase + 'key?username=admin&password=Dst2013admin'
		);
		let fechaI = req.body.fechaI;
		let fechaF = req.body.fechaF;
		let unidades = req.body.unidades;
		console.log(unidades);
		let consulta = await DBVehiculos.historico(
			urlBase,
			key,
			unidades,
			fechaI,
			fechaF
		);
		res.status(202).json(consulta);
	} catch (error) {
		res.status(500).json(error);
	}
};
module.exports = estaticoController;
