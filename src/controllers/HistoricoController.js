var historicoController = {};
const DBVehiculos = require('../models/Vehiculos');
const DBTraccar = require('../models/Traccars.js');
const mapaMiddleware = require('../middlewares/mapa');
const DBkapok = require('../models/kapok');
const turf = require('../public/js/turf.min.js');

historicoController.index = async function (req, res) {
	try {
		let fullUrl = req.protocol + '://' + req.get('Host') + req.originalUrl;
		let urlBase = mapaMiddleware.url(fullUrl);
		let key = await DBVehiculos.key(urlBase + 'key?username=admin&password=Dst2013admin');
		let unidades = await DBVehiculos.listUnidades(urlBase, key);
		let traccardevices = await DBTraccar.getDevices();
		res.render('historico.html', { unidades: unidades, traccardevices: traccardevices });
	} catch (e) {
		console.log('Error', e);
	}
};

historicoController.consulta = async function (req, res) {
	try {
		let fullUrl = req.protocol + '://' + req.get('Host') + req.originalUrl;
		let urlBase = mapaMiddleware.url(fullUrl);
		let key = await DBVehiculos.key(urlBase + 'key?username=admin&password=Dst2013admin');
		let fechaI = req.body.fechaI;
		let fechaF = req.body.fechaF;
		let unidades = req.body.unidades;
		let consulta = await DBVehiculos.historico(urlBase, key, unidades, fechaI, fechaF);
		res.status(202).json(consulta);
	} catch (error) {
		res.status(500).json(error);
	}
};

historicoController.consultaTraccar = async function (req, res) {
	try {
		let fechaI = req.body.fechaI;
		let fechaF = req.body.fechaF;
		let deviceid = req.body.unidades;
		let rs = await DBTraccar.historico(deviceid, fechaI, fechaF);
		res.status(202).json(rs);
	} catch (e) {
		res.status(500).json(e);
	}
};

historicoController.consultaAll = async function (req, res) {
	try {
		/*Variables*/
		let fechaI = req.body.fechaI;
		let fechaF = req.body.fechaF;
		let poligono = req.body.poligono;
		poligono = JSON.parse(poligono);
		let searchType = req.body.searchType;

		/*Datos para realizar la busqueda*/
		let resultado2 = [];
		let gps, polig, isDentro;
		polig = turf.polygon([poligono]);

		if (searchType == 2 || searchType == 3) {
			console.log('Iniciando busqueda de unidades');
			/*Pedimos url y llave de ceiba*/
			let fullUrl = req.protocol + '://' + req.get('Host') + req.originalUrl;
			let urlBase = mapaMiddleware.url(fullUrl);
			let key = await DBVehiculos.key(urlBase + 'key?username=admin&password=Dst2013admin');

			/*Traemos todos los terids de las unidades*/
			let terids = await DBVehiculos.listUnidades(urlBase, key);

			/*Traemos un arreglo de los GPS de las unidades*/
			let resultado = [];
			let consulta;

			//Iteramos cada vehiculo para saber su historial de posiciones//
			for (let i = 0; i < terids.length; i++) {
				consulta = await DBVehiculos.historico(urlBase, key, terids[i].deviceid, fechaI, fechaF);
				//console.log("Consulta" +i, consulta);
				if (consulta.length > 0) {
					resultado.push({ id: terids[i].deviceid, unidad: terids[i].carlicence, ubicaciones: consulta });
				}
			}

			//Iteramos las posiciones de las unidades para ver cuales se encuentra dentro del poligono//
			for (let i = 0; i < resultado.length; i++) {
				for (let j = 0; j < resultado[i].ubicaciones.length; j++) {
					gps = turf.point([resultado[i].ubicaciones[j].gpslat, resultado[i].ubicaciones[j].gpslng]);
					isDentro = turf.booleanPointInPolygon(gps, polig);
					if (isDentro) {
						resultado2.push({ terid: resultado[i].id, unidad: resultado[i].unidad, time: resultado[i].ubicaciones[j].gpstime });
					}
				}
			}
			console.log('Terminando busqueda de unidades');
		}
		if (searchType == 1 || searchType == 3) {
			console.log('Iniciando busqueda de dispositivos');
			/*Traemos un arreglo de los GPS de los dispositivos del traccar */
			let traccarDevicesGPS;
			traccarDevicesGPS = await DBTraccar.historicos(fechaI, fechaF);
			/*Arreglo para determinar si los dispositivos del traccar estan dentro del poligono*/
			let tablet;
			if (traccarDevicesGPS.length > 0) {
				for (let i = 0; i < traccarDevicesGPS.length; i++) {
					gps = turf.point([traccarDevicesGPS[i].latitude, traccarDevicesGPS[i].longitude]);
					isDentro = turf.booleanPointInPolygon(gps, polig);
					if (isDentro) {
						tablet = await DBTraccar.getNameTablet(traccarDevicesGPS[i].deviceid);
						resultado2.push({ terid: traccarDevicesGPS[i].deviceid, unidad: tablet[0].name, time: traccarDevicesGPS[i].devicetime });
					}
				}
			}
			console.log('Terminando busqueda de dispositivos');
		}

		res.status(202).json(resultado2);
	} catch (error) {
		console.log(error);
		res.status(500).json(error);
	}
};
module.exports = historicoController;
