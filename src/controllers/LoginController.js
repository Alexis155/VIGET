const passport = require("passport");
const LocalStrategy = require("passport-local").Strategy;
const usuarios = require("../models/Usuarios");
const { permisos } = require("../middlewares/login");

passport.use(
  "local.login",
  new LocalStrategy(
    {
      usernameField: "usuario",
      passwordField: "password",
      passReqToCallback: true,
    },
    async (req, usuario, password, done) => {
      const row = await usuarios.obtenerUsuario(usuario);
      if (row.recordset.length > 0) {
        let per = permisos(row.recordset);
        let user = row.recordset[0];
        user.CveSubMod = per;
        user.PasswdUsr = user.PasswdUsr.trim();
        if (user.SituEmp === "A") {
          if (user.PasswdUsr == password) {
            if (user.CveModulo === "VIGET" && user.HabilModul === "S" && user.Acceso === "S") {
              done(null,user,req.flash("success", "Bienvenido " + user.NombreUsr));
            } else {
              done(null,false,req.flash("error", "No tienes acceso al sistema"));
            }
          } else {
            done(null,false,req.flash("error", "Usuario y/o Contraseña incorrectos"));
          }
        } else {
          done(null, false, req.flash("error", "Empleado dado de baja"));
        }
      } else {
        done(null,false,req.flash("error", "Usuario y/o Contraseña incorrectos"));
      }
    }
  )
);

passport.serializeUser((user, done) => {
  done(null, user);
});

passport.deserializeUser(function (user, done) {
  done(null, user);
});

