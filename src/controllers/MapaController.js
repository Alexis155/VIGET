var mapaController = {};
const DBregiones = require('../models/VvigetGposoper');
const DBcamaras = require('../models/VvigetCamaras');
const DBtraccar = require('../models/Traccars');
const DBVehiculos = require('../models/Vehiculos');
const DBkapok = require('../models/kapok');
const DBRegLimits = require('../models/RegionDelim');
const DBVigetAlertas = require('../models/VigetAlertas');
const DBEmergencias = require('../models/VEmergenciasActivasViget');
const DBCentros = require('../models/CentrosDeSalud');
const DBLuminarias = require('../models/Luminarias');
const DBBotonoes = require('../models/Botones');
const mapaMiddleware = require('../middlewares/mapa');
var urlBase;

mapaController.index = async function (req, res) {
	try {
		let Listaregiones = await DBregiones.findAll();
		let lat = null;
		let long = null;
		res.render('mapaC.html', {
			regiones: Listaregiones.recordset,
			lat: lat,
			long: long,
			user: req.user,
		});
	} catch (err) {
		console.log('Error', err);
	}
};

mapaController.indexP = async function (req, res) {
	try {
		let Listaregiones = await DBregiones.findAll();
		let lat = req.params.lat;
		let long = req.params.long;
		res.render('mapaC.html', {
			regiones: Listaregiones.recordset,
			lat: lat,
			long: long,
		});
	} catch (err) {
		console.log('Error', err);
	}
};

mapaController.camaras = async function (req, res) {
	try {
		let camaras = await DBcamaras.findAll();
		res.status(202).json({ camaras: camaras.recordset });
	} catch (error) {
		console.log(error);
		res.status(505).json({
			mensaje: '¡Algo salió mal, intentelo de nuevo!',
			estatus: 'error',
			error: error,
		});
	}
};

mapaController.botones = async function (req, res) {
	try {
		let botones = await DBBotonoes.findAll();
		res.status(202).json({ botones: botones.recordset });
	} catch (error) {
		console.log(error);
		res.status(505).json({
			mensaje: '¡Algo salió mal, intentelo de nuevo!',
			estatus: 'error',
			error: error,
		});
	}
};

mapaController.traccar = async function (req, res) {
	try {
		let reg = req.body.reg;
		let tabletas = await DBtraccar.findAll();
		res.status(202).json({ tabletas: tabletas });
	} catch (error) {
		console.log(error);
		res.status(505).json({
			mensaje: '¡Algo salió mal, intentelo de nuevo!',
			estatus: 'error',
			error: error,
		});
	}
};

mapaController.vehiculos = async function (req, res) {
	try {
		let patrullas = await DBkapok.Unidades();
		res.status(202).json({ vehiculos: patrullas.recordset });
	} catch (error) {
		//console.log('Error: ', error);
	}
};

mapaController.gpsEstado = async function (req, res) {
	try {
		let gpsE = await DBVehiculos.gpsEstado();
		res.status(202).json(gpsE);
	} catch (error) {
		res.status(500).json({ error: error });
		console.log(error);
	}
};

mapaController.listaVehiculos = async function (req, res) {
	try {
		let reg = req.body.reg;
		let unidades;
		if (reg != null && reg != 'null' && req.body.reg) {
			unidades = await DBVehiculos.ListaUnidadesRegion(reg);
		} else {
			unidades = await DBVehiculos.ListaUnidades();
		}
		res.status(202).json(unidades.recordset);
	} catch (err) {
		console.log('Error: ', err);
		res.status(505).json({
			mensaje: '¡Algo salió mal, intentelo de nuevo!',
			estatus: 'error',
			error: err,
		});
	}
};

mapaController.regiones = async function (req, res) {
	try {
		let regiones = await DBregiones.findAll();
		res.status(202).json(regiones.recordset);
	} catch (error) {
		console.log('Error: ', error);
		res.status(505).json({
			mensaje: '¡Algo salió mal, intentelo de nuevo!',
			estatus: 'error',
			error: err,
		});
	}
};

mapaController.lectores = async function (req, res) {
	try {
		let listado, activos;
		listado = await DBVehiculos.listadoLectores();
		activos = await DBVehiculos.lectoresActivos();
		res.status(202).json({
			listado: listado.recordset,
			activos: activos.recordset,
		});
	} catch (e) {
		console.log('Error: ', e);
		res.status(505).json({
			mensaje: '¡Algo ha salido mal, intentelo de nuevo!',
			estatus: 'error',
			error: e,
		});
	}
};

mapaController.infoUnidad = async function (req, res) {
	try {
		let personal = await DBVehiculos.infoPerosnal(req.body.CveVehi);
		let equipo = await DBVehiculos.equipoUnidad(req.body.CveVehi);
		let unidad = await DBVehiculos.unidadId(req.body.CveVehi);
		res.status(200).json({
			personal: personal.recordset,
			equipo: equipo.recordset,
			unidad: unidad.recordset,
		});
	} catch (error) {
		console.log('Error: ', error);
	}
};

mapaController.limitesRegiones = async function (req, res) {
	try {
		let limites = await DBRegLimits.LimitiesRegines();
		res.status(200).json({ limites: limites.recordset });
	} catch (error) {
		console.log('Error: ', error);
	}
};

mapaController.insertAlert = async function (req, res) {
	try {
		let alertas = req.body.alertas;
		let alerta;
		for (let index = 0; index < alertas.length; index++) {
			alerta = await DBVigetAlertas.verify(alertas[index].CveVehi, alertas[index].fecha);
			if (alerta.recordset.length == 0) {
				await DBVigetAlertas.insert(alertas[index].CveVehi, alertas[index].fecha, alertas[index].regO, alertas[index].regA);
			}
		}
		res.status(200).json({ mensaje: 'ok' });
	} catch (error) {
		console.log('error', error);
	}
};

mapaController.emergencias = async function (req, res) {
	try {
		let emergencias = await DBEmergencias.emergencias();

		res.status(200).json(emergencias.recordset);
	} catch (error) {
		console.log(error);
	}
};
mapaController.reporteGPS = async function (req, res) {
	try {
		let patrullas = await DBkapok.Unidades();
		var sinReporte = new Array();
		let hoy = new Date();
		for (var i = 0; i < patrullas.recordset.length; i++) {
			let gpst = new Date(patrullas.recordset[i].gpstime);
			let transcurso = hoy - gpst;
			if (transcurso > 360000) {
				sinReporte.push(patrullas.recordset[i]);
			}
		}
		res.render('reporteGPS.html', { patrullas: sinReporte });
	} catch (err) {
		console.log('Error: ', err);
	}
};
mapaController.centrosSalud = async function (req, res) {
	try {
		let centros = await DBCentros.findAll();
		res.status(200).json(centros.recordset);
	} catch (error) {
		res.status(500).json(error);
	}
};
mapaController.luminarias = async function (req, res) {
	try {
		let _ch = await DBLuminarias.findCH();
		let _eg = await DBLuminarias.findEG();
		let _fcp = await DBLuminarias.findFCP();
		let _fo = await DBLuminarias.findFO();
		let _jvh = await DBLuminarias.findJVH();
		let _srj = await DBLuminarias.findSRJ();
		let _vcr = await DBLuminarias.findVCR();
		let _vialidades = await DBLuminarias.findVIALIDADES();
		res.status(200).json({
			ch: _ch.recordset,
			eg: _eg.recordset,
			fcp: _fcp.recordset,
			fo: _fo.recordset,
			jvh: _jvh.recordset,
			srj: _srj.recordset,
			vcr: _vcr.recordset,
			vialidades: _vialidades.recordset,
		});
	} catch (error) {
		console.log(error);
		res.status(500).json(error);
	}
};

module.exports = mapaController;
