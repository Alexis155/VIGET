const DBVideoAbordo = require('../models/VideoAbordo');
const DBVehiculos = require('../models/Vehiculos');
const mapaMiddleware = require('../middlewares/mapa');
const DBBloqueo = require('../models/kapok');
var VideoController = {};

VideoController.index = async function (req, res) {
	try {
		var idUnidad = req.params.idUnidad;
		var unidad = req.params.unidad;
		res.render('videosUnidad.html', { idUnidad, unidad });
	} catch (error) {
		console.log(error);
		res.status(505).json({
			mensaje: '¡Algo salió mal, intentelo de nuevo!',
			estatus: 'error',
		});
	}
};
VideoController.insert = async function (req, res) {
	try {
		let CveUsuario = req.body.CveUsuario;
		let CveVehi = req.body.CveVehi;
		await DBVideoAbordo.insert(CveUsuario, CveVehi);
	} catch (error) {
		console.log('Error:', error);
	}
};
VideoController.update = async function (req, res) {
	try {
		let CveUsuario = req.body.CveUsuario;
		let error = req.body.error;
		if (error) {
			await DBVideoAbordo.updateError(CveUsuario);
		} else {
			await DBVideoAbordo.update(CveUsuario);
		}
	} catch (error) {
		console.log('Error:', error);
	}
};
VideoController.ObnterVideo = async function (req, res) {
	try {
		let urlBase = mapaMiddleware.url(req.body.estatus);
		let urlkey = mapaMiddleware.urlVideo(
			urlBase,
			req.body.user,
			req.body.pass,
			null,
			'key'
		);
		let key = await DBVehiculos.key(urlkey);
		let video = await DBVehiculos.videoVehiculo(
			urlBase,
			key,
			req.body.terId,
			req.body.canal
		);
		video = mapaMiddleware.urlVideo(urlBase, null, null, video, 'video');
		res.status(202).json({ video: video });
	} catch (error) {
		console.log('Error:', error);
		res.json({ error: error });
	}
};
VideoController.bloqueo = async function (req, res) {
	try {
		let CveVehi = req.body.CveVehi;
		let bloqueo = await DBBloqueo.Bloqueo(CveVehi);
		res.status(200).json(bloqueo.recordset);
	} catch (error) {
		res.status(500).json(error);
	}
};
module.exports = VideoController;
