//Imports
const express = require('express');
var bodyParser = require('body-parser');
const morgan = require('morgan');
const path = require('path');
const flash = require('connect-flash');
const session = require('express-session');
const passport = require('passport');
require('events').EventEmitter.prototype._maxListeners = Infinity;

//Initializations
const app = express();
require('./controllers/LoginController');
//Settings
//Toma el puerto disponible y en caso que no toma el 4000
app.set('port', process.env.PORT || 4000);
//Le damos a la aplicación la dirección de la carpeta views
app.set('views', path.join(__dirname, 'views'));
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'ejs');

//-------------------Middlewares | Peticiones-----------------------------
app.use(
	session({
		secret: 'sspmqsessionviget',
		resave: false,
		saveUninitialized: false,
		// cookie: { maxAge: 2.88e+7 }
	})
);
app.use(flash());
app.use(morgan('dev'));
// app.use(bodyParser.urlencoded({ extended: false })); //Solo aceptamos datos sencillos
app.use(bodyParser.json({ limit: '50mb' })); //Preparamos el app para envio y rececpion de JSON
app.use(bodyParser.urlencoded({ limit: '50mb', extended: false }));
app.use(passport.initialize());
app.use(passport.session());

//--------------------Global Variables
app.use((req, res, next) => {
	app.locals.error = req.flash('error');
	app.locals.user = req.user;
	next();
});
//-------------------------Routes
app.use(require('./routes/camaras'));
app.use(require('./routes/mapa'));
app.use(require('./routes/login'));
app.use(require('./routes/videosUnidad'));
app.use(require('./routes/asignacion'));
app.use(require('./routes/historico'));
app.use(require('./routes/estatico'));

//-------------------------Public
app.use(express.static(path.join(__dirname, 'public')));

//-------------------------Starting the server
app.listen(app.get('port'), () => {
	console.log('Server on port ', app.get('port'));
});
