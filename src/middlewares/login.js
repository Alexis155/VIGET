module.exports = {
	isLoggedIn(req, res, next) {
		if (req.isAuthenticated()) {
			return next();
		}
		return res.redirect('/');
	},
	isNotLoggedIn(req, res, next) {
		if (!req.isAuthenticated()) {
			return next();
		} else {
			return res.redirect('/mapa');
		}
	},
	permisos(usuario) {
		var permisos = [];
		for (let i = 0; usuario.length > i; i++) {
			permisos.push({ permiso: usuario[i].CveSubMod });
		}
		return permisos;
	},
};
