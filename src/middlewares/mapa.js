const { map } = require("mssql");

var mapa = {};
mapa.url = function (url) {
  if (url.search("https://viget.sspmqro.gob.mx") != -1) {
    return "https://sspmqro.gob.mx/videoabordo/api/v1/basic/";
  } else {
    return "http://172.23.176.22:12056/api/v1/basic/";
  }
};

mapa.urls = function (urlBase, key, type) {
  switch (type) {
    case "key":
      return urlBase + "key?username=admin&password=Dst2013admin";
      break;
    case "vehiculo":
      return urlBase + "devices?key=" + key;
      break;
    case "gps":
      return urlBase + "gps/last";
      break;
  }
};
mapa.urlVideo = function (urlBase, user, pass, video, type) {
  let vid;
  switch (type) {
    case "key":
      return urlBase + "key?username=" + user.trim() + "&password=" + pass;
      break;
    case "video":
      if (
        urlBase.search("https://sspmqro.gob.mx/videoabordo/api/v1/basic/") != -1
      ) {
        vid = video.replace(
          "http://172.23.176.22:12060",
          "https://sspmqro.gob.mx/videolive"
        );
        return vid;
      } else {
        return video;
      }
      break;
  }
};
mapa.ObetnerDevicesID = function (devicesid) {
  let devicesIds = new Array();
  for (let x in devicesid) {
    devicesIds.push(devicesid[x].deviceid);
  }
  return devicesIds;
};
mapa.UnirInfo = function (gpsVehiculos, devicesid) {
  let patrullas = new Array();
  gpsVehiculos.forEach(function (kapok) {
    let union = devicesid.filter(
      (vehiculo) => vehiculo.deviceid == kapok.terid
    );
    if (union[0] != null) {
      patrullas.push({
        terid: kapok.terid,
        carlicence: union[0].carlicence,
        gpstime: kapok.gpstime,
        altitude: kapok.altitude,
        direction: kapok.direction,
        gpslat: kapok.gpslat,
        gpslng: kapok.gpslng,
        speed: kapok.speed,
        recordspeed: kapok.recordspeed,
        state: kapok.state,
        time: kapok.time,
        regionId: union[0].groupid,
      });
    }
  });
  return patrullas;
};

module.exports = mapa;
