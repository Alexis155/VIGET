const sql = require("mssql");
const conection = require("../BD/database");

const findAll = () => {
  return new Promise((resolve, reject) => {
    conection.obtenerPool().then((pool) => {
        return pool
          .request()
          .query(
            "SELECT [id] ,[DeviceId] ,[nombre] ,[latitud] ,[longitud] FROM [BDCata].[dbo].[Botones]"
          );
      }).then((result) => {
        resolve(result);
        conection.cerrarPool();
      }).catch((err) => {
        reject(err);
      });
  });
};

const findId = (id) => {
  return new Promise((resolve, reject) => {
    conection.obtenerPool().then((pool) => {
      return pool.request().input(
        "id", sql.Int, id).query("SELECT [id] ,[DeviceId] ,[nombre] ,[latitud] ,[longitud] FROM [BDCata].[dbo].[Botones] WHERE id =@id");
      }).then((result) => {
        resolve(result);
        conection.cerrarPool();
      }).catch((err) => {
        reject(err);
      });
  });
}
module.exports = {
  findAll,
  findId
}