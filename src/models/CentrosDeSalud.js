const sql = require("mssql");
const conection = require("../BD/database");

const findAll = () => {
  return new Promise((resolve, reject) => {
    conection.obtenerPool().then((pool) => {
        return pool
          .request()
          .query(
            "SELECT [municipio] ,[nombre] ,[Latitud] ,[Longitud] FROM [BDCata].[dbo].[CentrosDeSalud]"
          );
      }).then((result) => {
        resolve(result);
        conection.cerrarPool();
      }).catch((err) => {
        reject(err);
      });
  });
};

module.exports = {
  findAll,
}