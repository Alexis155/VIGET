const sql = require("mssql");
const conection = require("../BD/database");

const findAll = () => {
  return new Promise((resolve, reject) => {
    conection
      .obtenerPool()
      .then((pool) => {
        return pool
          .request()
          .query(
            "SELECT CveNumEmp, full_name,CveGpoOper  FROM [BDCope].[dbo].[VVIGET_PERSONAL]"
          );
      })
      .then((result) => {
        resolve(result);
        conection.cerrarPool();
      })
      .catch((err) => {
        reject(err);
      });
  });
};
const findId = (cveNumEmp) => {
  return new Promise((resolve, reject) => {
    conection
      .obtenerPool()
      .then((pool) => {
        return pool
          .request()
          .input("CveNumEmp", sql.Int, cveNumEmp)
          .query(
            "SELECT [CveVehi],[consecutivo],[ano_asignacion] FROM [BDCope].[dbo].[VVIGET_ASIGNACION_UNIDADES] Where [CveNumEmp] = @CveNumEmp AND [Estatus] = 1"
          );
      })
      .then((result) => {
        resolve(result);
        conection.cerrarPool();
      })
      .catch((err) => {
        reject(err);
      });
  });
}
const findLast = () => {
  return new Promise((resolve, reject) => {
    conection
      .obtenerPool()
      .then((pool) => {
        return pool
          .request()
          .query(
            "SELECT top 1 [ano_asignacion],[consecutivo] FROM [BDCope].[dbo].[VIGET_ASIGNACION] order by fecha_asignacion_ini desc"
          );
      })
      .then((result) => {
        resolve(result);
        conection.cerrarPool();
      })
      .catch((err) => {
        reject(err);
      });
  });
}
const finAllAsignaciones = (cveVehi) => {
  return new Promise((resolve, reject) => {
    conection
      .obtenerPool()
      .then((pool) => {
        return pool
          .request()
          .input("CveVehi", sql.VarChar, cveVehi)
          .query(
            "SELECT [CveNumEmp],[nombre],[estatus],[CveVehi],[consecutivo],[ano_asignacion] FROM [BDCope].[dbo].[VVIGET_ASIGNACION_UNIDADES] Where [CveVehi] = @CveVehi  ORDER BY ano_asignacion DESC, consecutivo DESC"
          );
      })
      .then((result) => {
        resolve(result);
        conection.cerrarPool();
      })
      .catch((err) => {
        reject(err);
      });
  });
}
const insert = (cveVehi, cveNumEmp, cveEmpAsigna, consecutivo) => {
  return new Promise((resolve, reject) => {
    let insert = "INSERT INTO [BDCope].[dbo].[VIGET_ASIGNACION] ([CveVehi],[CveNumEmp],[CveEmpAsigna],[fecha_asignacion_ini],[fecha_asignacion_fin],[estatus],[ano_asignacion],[consecutivo]) values (" +
      "@CveVehi," +
      "@CveNumEmp," +
      "@CveEmpAsigna," +
      "CURRENT_TIMESTAMP,null,1," +
      "YEAR(CURRENT_TIMESTAMP)," +
      "@consecutivo)";
    conection.obtenerPool().then((pool) => {
      const transaccion = new sql.Transaction(pool);
      transaccion.begin((err) => {
        let rolledBack = false;
        transaccion.on("rollback", (aborted) => {
          rolledBack = true;
        });
        new sql.Request(transaccion)
          .input("CveVehi", sql.VarChar, cveVehi)
          .input("CveNumEmp", sql.Int, cveNumEmp)
          .input("CveEmpAsigna", sql.Int, cveEmpAsigna)
          .input("consecutivo", sql.Int, consecutivo)
          .query(insert, (err, result) => {
            if (err) {
              if (!rolledBack) {
                transaccion.rollback((err) => {});
                reject(err);
              }
            } else {
              transaccion.commit((err) => {
                if (err) {
                } else {
                  resolve("Inserción correcta");
                }
              });
            }
          });
      });
    });
  });
}
const update = (consecutivo, anoAsignacion) => {
  return new Promise((resolve, reject) => {
    conection
      .obtenerPool()
      .then((pool) => {
        return pool
          .request()
          .input("consecutivo", sql.Int, consecutivo)
          .input("ano", sql.Int, anoAsignacion)
          .query(
            " UPDATE[BDCope].[dbo].[VIGET_ASIGNACION] " +
              "SET estatus = 0, fecha_asignacion_fin = CURRENT_TIMESTAMP " +
              "WHERE consecutivo = @consecutivo AND ano_asignacion = @ano"
          );
      })
      .then((result) => {
        resolve(result);
        conection.cerrarPool();
      })
      .catch((err) => {
        reject(err);
      });
  });
}
const obtenerUltimaAsginacion = () => {
  return new Promise((resolve, reject) => {
    conection
      .obtenerPool()
      .then((pool) => {
        return pool
          .request()
          .query(
            "SELECT top 1 [ano_asignacion],[consecutivo] FROM [BDCope].[dbo].[VIGET_ASIGNACION] order by fecha_asignacion_ini desc"
          );
      })
      .then((result) => {
        resolve(result);
        conection.cerrarPool();
      })
      .catch((err) => {
        reject(err);
      });
  });
}
const asignarEmpleado = (cveNumEmp, cveVehi) => {
  return new Promise((resolve, reject) => {
    obtenerConsecutivoAsignacion()
      .then((consecutivo) => {
        return asignacionesBD.insertarAsignacion(
          cveVehi,
          cveNumEmp,
          27701,
          consecutivo
        );
      })
      .then((res) => {
        resolve(true);
      })
      .catch((error) => {
        reject(false);
      });
  });
};
const obtenerConsecutivoAsignacion = () => {
  return new Promise((resolve, reject) => {
    let fechaActual = new Date();
    let anoActual = fechaActual.getFullYear();
    asignacionesBD
      .obtenerUltimaAsginacion()
      .then((data) => {
        let registro = data.recordset[0];
        let consecutivo = 0;
        if (registro.ano_asignacion == anoActual) {
          consecutivo = parseInt(registro.consecutivo) + 1;
        } else {
          consecutivo = 1;
        }
        resolve(consecutivo);
      })
      .catch((err) => {
        reject(err);
      });
  });
}
module.exports = {
  findAll,
  findId,
  findLast,
  finAllAsignaciones,
  insert,
  update,
  asignarEmpleado,
  obtenerUltimaAsginacion
}