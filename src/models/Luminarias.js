const sql = require("mssql");
const conection = require("../BD/database");

const findCH= () => {
  return new Promise((resolve, reject) => {
    conection.obtenerPool().then((pool) => {
        return pool
          .request()
          .query(
            "SELECT [Delegacion] ,[Latitud] ,[Longitud] FROM [BDCata].[dbo].[Luminarias] WHERE Delegacion = 'CH_LED_SERIES_KML'"
          );
      }).then((result) => {
        resolve(result);
        conection.cerrarPool();
      }).catch((err) => {
        reject(err);
      });
  });
};
const findEG= () => {
  return new Promise((resolve, reject) => {
    conection.obtenerPool().then((pool) => {
        return pool
          .request()
          .query(
            "SELECT [Delegacion] ,[Latitud] ,[Longitud] FROM [BDCata].[dbo].[Luminarias] WHERE Delegacion = 'EG_KML'"
          );
      }).then((result) => {
        resolve(result);
        conection.cerrarPool();
      }).catch((err) => {
        reject(err);
      });
  });
};
const findFCP= () => {
  return new Promise((resolve, reject) => {
    conection.obtenerPool().then((pool) => {
        return pool
          .request()
          .query(
            "SELECT [Delegacion] ,[Latitud] ,[Longitud] FROM [BDCata].[dbo].[Luminarias] WHERE Delegacion = 'FCP_KML'"
          );
      }).then((result) => {
        resolve(result);
        conection.cerrarPool();
      }).catch((err) => {
        reject(err);
      });
  });
};
const findFO= () => {
  return new Promise((resolve, reject) => {
    conection.obtenerPool().then((pool) => {
        return pool
          .request()
          .query(
            "SELECT [Delegacion] ,[Latitud] ,[Longitud] FROM [BDCata].[dbo].[Luminarias] WHERE Delegacion = 'FO_KML'"
          );
      }).then((result) => {
        resolve(result);
        conection.cerrarPool();
      }).catch((err) => {
        reject(err);
      });
  });
};
const findJVH= () => {
  return new Promise((resolve, reject) => {
    conection.obtenerPool().then((pool) => {
        return pool
          .request()
          .query(
            "SELECT [Delegacion] ,[Latitud] ,[Longitud] FROM [BDCata].[dbo].[Luminarias] WHERE Delegacion = 'JVH_KML'"
          );
      }).then((result) => {
        resolve(result);
        conection.cerrarPool();
      }).catch((err) => {
        reject(err);
      });
  });
};
const findSRJ= () => {
  return new Promise((resolve, reject) => {
    conection.obtenerPool().then((pool) => {
        return pool
          .request()
          .query(
            "SELECT [Delegacion] ,[Latitud] ,[Longitud] FROM [BDCata].[dbo].[Luminarias] WHERE Delegacion = 'SRJ_KML'"
          );
      }).then((result) => {
        resolve(result);
        conection.cerrarPool();
      }).catch((err) => {
        reject(err);
      });
  });
};
const findVCR= () => {
  return new Promise((resolve, reject) => {
    conection.obtenerPool().then((pool) => {
        return pool
          .request()
          .query(
            "SELECT [Delegacion] ,[Latitud] ,[Longitud] FROM [BDCata].[dbo].[Luminarias] WHERE Delegacion = 'VCR_KML'"
          );
      }).then((result) => {
        resolve(result);
        conection.cerrarPool();
      }).catch((err) => {
        reject(err);
      });
  });
};
const findVIALIDADES= () => {
  return new Promise((resolve, reject) => {
    conection.obtenerPool().then((pool) => {
        return pool
          .request()
          .query(
            "SELECT [Delegacion] ,[Latitud] ,[Longitud] FROM [BDCata].[dbo].[Luminarias] WHERE Delegacion = 'VIALIDADES_KML'"
          );
      }).then((result) => {
        resolve(result);
        conection.cerrarPool();
      }).catch((err) => {
        reject(err);
      });
  });
};
module.exports = {
  findCH,
  findEG,
  findFCP,
  findFO,
  findJVH,
  findSRJ,
  findVCR,
  findVIALIDADES
}