const sql = require("mssql");
const conection = require("../BD/database");

const LimitiesRegines = () => {
  return new Promise((resolve, reject) => {
    conection.obtenerPool().then((pool) => {
        return pool.request().query("SELECT  [BDCata].[dbo].[RegionDelim].[CveRegion], [RegionDescripcion], [DelimLat], [DelimLong], [RegionColor], [Id_kapok] FROM [BDCata].[dbo].[RegionDelim] LEFT JOIN [BDCata].[dbo].[Regiones] ON [BDCata].[dbo].[RegionDelim].[CveRegion] = [BDCata].[dbo].[Regiones].CveRegion");
      }).then((result) => {
        resolve(result);
        conection.cerrarPool();
      }).catch((err) => {
        reject(err);
      });
  });
}

module.exports = {
  LimitiesRegines,
}