const conection = require('../BD/databaseMariaDB');

const memoize = (fn) => {
	//Creamos el cache para los resultados
	const cache = {};
	//Retornamos una funcion
	return (...args) => {
		//Creamos una llave para nuestro cache
		const stringifiedArgs = JSON.stringify(args);
		//Solo se ejecuta si no tenemos algo en el cache
		const result = (cache[stringifiedArgs] = typeof cache[stringifiedArgs] === 'undefined' ? fn(...args) : cache[stringifiedArgs]);
		return result;
	};
};

const findAll = () => {
	var query = 'SELECT * FROM traccar.VUltimaPosicionTabletas ORDER BY status desc';
	return new Promise((resolver, reject) => {
		conection
			.obtenerConexion()
			.then((con) => {
				con.end();
				return con.query(query);
			})
			.then((result) => {
				resolver(result);
			})
			.catch((err) => {
				reject(err);
			});
	});
};

const getDevices = () => {
	var query = 'SELECT * FROM traccar.devices ORDER BY name ASC';
	return new Promise((resolver, reject) => {
		conection
			.obtenerConexion()
			.then((con) => {
				con.end();
				return con.query(query);
			})
			.then((rs) => {
				resolver(rs);
			})
			.catch((e) => {
				reject(e);
			});
	});
};

const historico = (deviceid, from, to) => {
	var query =
		'SELECT id, deviceid, latitude, longitude, devicetime FROM `traccar`.`positions`' +
		'WHERE deviceid=' +
		deviceid +
		" AND devicetime BETWEEN '" +
		from +
		"' AND '" +
		to +
		"';";
	return new Promise((resolver, reject) => {
		conection
			.obtenerConexion()
			.then((con) => {
				con.end();
				return con.query(query);
			})
			.then((rs) => {
				resolver(rs);
			})
			.catch((e) => {
				reject(e);
			});
	});
};

const historicos = (from, to) => {
	var query =
		'SELECT id, deviceid, latitude, longitude, devicetime FROM `traccar`.`positions`' +
		"WHERE devicetime BETWEEN '" +
		from +
		"' AND '" +
		to +
		"';";
	return new Promise((resolver, reject) => {
		conection
			.obtenerConexion()
			.then((con) => {
				con.end();
				return con.query(query);
			})
			.then((rs) => {
				resolver(rs);
			})
			.catch((e) => {
				reject(e);
			});
	});
};

const getNameTablet = (id) => {
	var query = 'SELECT * FROM `traccar`.`devices` WHERE id=' + id + ';';
	return new Promise((resolver, reject) => {
		conection
			.obtenerConexion()
			.then((con) => {
				con.end();
				return con.query(query);
			})
			.then((rs) => {
				resolver(rs);
			})
			.catch((e) => {
				reject(e);
			});
	});
};

module.exports = { findAll, getDevices, historico, historicos, getNameTablet };
