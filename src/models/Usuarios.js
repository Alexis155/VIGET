const sql = require("mssql");
const conection = require("../BD/database");

const obtenerUsuario = (usuario) => {
  return new Promise((resolve, reject) => {
    conection
      .obtenerPool()
      .then((pool) => {
        return pool
          .request()
          .input("usuario", sql.VarChar, usuario)
          .query(
            "SELECT * FROM [BDAcce].[dbo].[VVigetAcces] WHERE [CveUsuario] =@usuario"
          );
      })
      .then((result) => {
        resolve(result);
        conection.cerrarPool();
      })
      .catch((err) => {
        console.log("error", err);
        reject(err);
      });
  });
};

module.exports = {
  obtenerUsuario,
};