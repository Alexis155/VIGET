const fetch = require('node-fetch');
const sql = require('mssql');
const conection = require('../BD/database');

const key = (url) => {
	return new Promise((resolve, reject) => {
		fetch(url, {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json',
			},
		})
			.then((res) => {
				res.json().then((data) => {
					let key = data.data.key;
					resolve(key);
				});
			})
			.catch((err) => {
				reject(err);
			});
	});
};

const listUnidades = (url, key) => {
	return new Promise((resolve, reject) => {
		fetch(url + 'devices?key=' + key, {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json',
			},
		})
			.then((res) => {
				res.json().then((data) => {
					let unidades = data.data;
					resolve(unidades);
				});
			})
			.catch((err) => {
				reject(err);
			});
	});
};

const historico = (url, key, terid, starttime, endtime) => {
	return new Promise((resolve, reject) => {
		fetch(url + 'gps/detail', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({
				key: key,
				terid: terid,
				starttime: starttime,
				endtime: endtime,
			}),
		})
			.then((res) => {
				res.json().then((data) => {
					let unidades = data.data;
					resolve(unidades);
				});
			})
			.catch((err) => {
				reject(err);
			});
	});
};

const gpsEstado = () => {
	return new Promise((resolve, reject) => {
		fetch('http://192.168.10.99:7001/Monitor/api/unidades', {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json',
				Authorization:
					'Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJNVU5JQ0lQSU8iLCJjbGllbnRfaWQiOiJBRkNCNDc2QjVGNTg0NjcyQUY5OEQ1NDI3OUIwRERBRiJ9.oXnJmZXOICyDAlsreMUr3P92HyQK5BNhWQNIB0g-pVbDgtyTUV850Du3J8p-6E0YhjxlgZHeukDSE9xwJQu3QA',
			},
		})
			.then((res) => {
				res.json().then((data) => {
					let dat = data;
					resolve(dat);
				});
			})
			.catch((err) => {
				console.log('error de gpsE', err);
				reject(err);
			});
	});
};

const videoVehiculo = (urlBase, key, terId, canal) => {
	return new Promise((resolve, reject) => {
		fetch(urlBase + 'live/video?key=' + key + '&terid=' + terId + '&chl=' + canal + '&audio=1&st=0&port=12060', {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json',
			},
		})
			.then((res) => {
				res.json().then((data) => {
					let url = data.data.url;
					resolve(url);
				});
			})
			.catch((err) => {
				reject(err);
			});
	});
};

const VvigetUnidades = (CveVehi) => {
	return new Promise((resolve, reject) => {
		conection
			.obtenerPool()
			.then((pool) => {
				return pool
					.request()
					.input('cve', sql.VarChar, CveVehi)
					.query(
						'SELECT CveGpoOper, CveVehi, PlacaVehi, CveMarcVeh, CveLinVehi FROM [BDCope].[dbo].[VVIGET_UNIDADES] Where [CveVehi] = @cve'
					);
			})
			.then((result) => {
				resolve(result);
				conection.cerrarPool();
			})
			.catch((err) => {
				reject(err);
			});
	});
};

const ListaUnidades = () => {
	return new Promise((resolve, reject) => {
		conection
			.obtenerPool()
			.then((pool) => {
				return pool
					.request()
					.query(
						"SELECT CveVehi,CveGpoOper, StatusVehi FROM [BDCope].[dbo].[VVIGET_UNIDADES] WHERE SituVehi = 'A' ORDER BY CveGpoOper ASC"
					);
			})
			.then((result) => {
				resolve(result);
				conection.cerrarPool();
			})
			.catch((err) => {
				reject(err);
			});
	});
};
const ListaUnidadesRegion = (reg) => {
	return new Promise((resolve, reject) => {
		conection
			.obtenerPool()
			.then((pool) => {
				return pool
					.request()
					.input('reg', sql.VarChar, reg)
					.query('SELECT CveVehi,CveGpoOper FROM [BDCope].[dbo].[VVIGET_UNIDADES] WHERE CveGpoOper = @reg');
			})
			.then((result) => {
				resolve(result);
				conection.cerrarPool();
			})
			.catch((err) => {
				reject(err);
			});
	});
};

const unidadId = (CveVehi) => {
	return new Promise((resolve, reject) => {
		conection
			.obtenerPool()
			.then((pool) => {
				return pool
					.request()
					.input('CveVehi', sql.VarChar, CveVehi)
					.query('SELECT * FROM [BDCope].[dbo].[VVIGET_UNIDADES] WHERE CveVehi = @CveVehi');
			})
			.then((result) => {
				resolve(result);
				conection.cerrarPool();
			})
			.catch((err) => {
				reject(err);
			});
	});
};

const infoPerosnal = (CveVehi) => {
	return new Promise((resolve, reject) => {
		conection
			.obtenerPool()
			.then((pool) => {
				return pool
					.request()
					.input('CveVehi', sql.VarChar, CveVehi)
					.query('SELECT * FROM [BDCope].[dbo].[VVIGET_ASIGNACION_UNIDADES] WHERE CveVehi = @CveVehi AND Estatus = 1');
			})
			.then((result) => {
				resolve(result);
				conection.cerrarPool();
			})
			.catch((err) => {
				reject(err);
			});
	});
};

const equipoUnidad = (CveVehi) => {
	return new Promise((resolve, reject) => {
		conection
			.obtenerPool()
			.then((pool) => {
				return pool
					.request()
					.input('CveVehi', sql.VarChar, CveVehi)
					.query('SELECT * FROM [BDCope].[dbo].[VEquipoPorUnidad] WHERE Unidad = @CveVehi ORDER BY EquipoOrden ASC');
			})
			.then((result) => {
				resolve(result);
				conection.cerrarPool();
			})
			.catch((err) => {
				reject(err);
			});
	});
};

const listadoLectores = () => {
	return new Promise((resolve, reject) => {
		conection
			.obtenerPool()
			.then((pool) => {
				return pool.request().query('SELECT * FROM [BDCata].[dbo].[UnidadesLectoresPlacas]');
			})
			.then((res) => {
				resolve(res);
				conection.cerrarPool();
			})
			.catch((err) => {
				reject(err);
			});
	});
};

const lectoresActivos = () => {
	return new Promise((resolve, reject) => {
		conection
			.connect()
			.then((pool) => {
				return pool.request().query('SELECT DISTINCT FCU FROM [EOC_TRAN].[dbo].[ListadoDeCamarasUltimos30Minutos]');
			})
			.then((res) => {
				resolve(res);
				conection.cerrarPool();
			})
			.catch((e) => {
				reject(e);
			});
	});
};

module.exports = {
	key,
	VvigetUnidades,
	ListaUnidades,
	ListaUnidadesRegion,
	infoPerosnal,
	equipoUnidad,
	unidadId,
	videoVehiculo,
	gpsEstado,
	listUnidades,
	historico,
	listadoLectores,
	lectoresActivos,
};
