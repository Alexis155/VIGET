const sql = require("mssql");
const conection = require("../BD/database");

const insert = (CveUsuario, CveVehi) => {
  return new Promise((resolve, reject) => {
    let insert = "INSERT INTO [BDAcce].[dbo].[VideoAbordo] ([CveUsuario],[VideoInicio],[VideoFin],[CveVehi]) values" +
      "(@CveUsuario, GETDATE(), null, @CveVehi)";
      conection.obtenerPool().then((pool) => {
        const transaccion = new sql.Transaction(pool);
        transaccion.begin((err) => {
          let rolledBack = false;
          transaccion.on("rollback", (aborted) => {
            rolledBack = true;
          });
          new sql.Request(transaccion)
            .input("CveUsuario", sql.Char, CveUsuario)
            .input("CveVehi", sql.VarChar, CveVehi)
            .query(insert, (err, result) => {
              if (err) {
                if (!rolledBack) {
                  transaccion.rollback((err) => {});
                  reject(err);
                }
              } else {
                transaccion.commit((err) => {
                  if (err) {
                  } else {
                    resolve("Inserción correcta");
                  }
                });
              }
            });
        });
      });
  });
}
const update = (CveUsuario) => {
  return new Promise((resolve, reject) => {
    let query = " UPDATE [BDAcce].[dbo].[VideoAbordo] SET VideoFin = GETDATE(), Estatus = 'Correcto' " +
    " where VideoInicio = (Select top 1  [VideoInicio] FROM [BDAcce].[dbo].[VideoAbordo] WHERE [CveUsuario] = @CveUsuario ORDER BY[VideoInicio] desc)";
    conection
      .obtenerPool()
      .then((pool) => {
        return pool
          .request()
          .input("CveUsuario", sql.Char, CveUsuario)
          .query(query);
      })
      .then((result) => {
        resolve(result);
        conection.cerrarPool();
      })
      .catch((err) => {
        reject(err);
      });
  });
}
const updateError = (CveUsuario) => {
  return new Promise((resolve, reject) => {
    let query = " UPDATE [BDAcce].[dbo].[VideoAbordo] SET VideoFin = GETDATE(), Estatus = 'Error' " +
    " where VideoInicio = (Select top 1  [VideoInicio] FROM [BDAcce].[dbo].[VideoAbordo] WHERE [CveUsuario] = @CveUsuario ORDER BY[VideoInicio] desc)";
    conection
      .obtenerPool()
      .then((pool) => {
        return pool
          .request()
          .input("CveUsuario", sql.Char, CveUsuario)
          .query(query);
      })
      .then((result) => {
        resolve(result);
        conection.cerrarPool();
      })
      .catch((err) => {
        reject(err);
      });
  });
}

module.exports = {
  insert,
  update,
  updateError
}