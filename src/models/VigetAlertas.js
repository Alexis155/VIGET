const sql = require('mssql');
const conection = require('../BD/database');

const all = () => {
	return new Promise((resolver, reject) => {
		conection
			.obtenerPool()
			.then((con) => {
				return con.request().query(' SELECT * FROM [BDCope].[dbo].[VigetAlertas]');
			})
			.then((result) => {
				resolver(result);
				con.cerrarPool();
			})
			.catch((err) => {
				reject(err);
			});
	});
};

const insert = (cveVehi, fecha, regO, regA) => {
	return new Promise((resolve, reject) => {
		let insert =
			'INSERT INTO [BDCope].[dbo].[VigetAlertas] ([CveVehi],[Fecha],[RegionOrigen],[RegionActal]) values (' +
			'@CveVehi,' +
			'@fecha,' +
			'@regO,' +
			'@regA)';
		conection.obtenerPool().then((pool) => {
			const transaccion = new sql.Transaction(pool);
			transaccion.begin((err) => {
				let rolledBack = false;
				transaccion.on('rollback', (aborted) => {
					rolledBack = true;
				});
				new sql.Request(transaccion)
					.input('CveVehi', sql.VarChar, cveVehi)
					.input('fecha', sql.DateTime, fecha)
					.input('regO', sql.VarChar, regO)
					.input('regA', sql.VarChar, regA)
					.query(insert, (err, result) => {
						if (err) {
							if (!rolledBack) {
								transaccion.rollback((err) => {});
								reject(err);
							}
						} else {
							transaccion.commit((err) => {
								if (err) {
								} else {
									resolve('Inserción correcta');
								}
							});
						}
					});
			});
		});
	});
};

const verify = (CveVehi, fecha) => {
	return new Promise((resolver, reject) => {
		conection
			.obtenerPool()
			.then((con) => {
				return con
					.request()
					.input('CveVehi', sql.VarChar, CveVehi)
					.input('fecha', sql.DateTime, fecha)
					.query('SELECT * FROM [BDCope].[dbo].[VigetAlertas] WHERE [CveVehi] = @CveVehi AND Fecha = @fecha');
			})
			.then((result) => {
				resolver(result);
				con.cerrarPool();
			})
			.catch((err) => {
				reject(err);
			});
	});
};

const count = () => {
	return new Promise((resolver, reject) => {
		conection
			.obtenerPool()
			.then((con) => {
				return con.request().query(' SELECT COUNT([CveVehi]) AS resultado FROM [BDCope].[dbo].[VigetAlertas]');
			})
			.then((result) => {
				resolver(result);
				con.cerrarPool();
			})
			.catch((err) => {
				reject(err);
			});
	});
};

module.exports = {
	insert,
	verify,
	count,
	all,
};
