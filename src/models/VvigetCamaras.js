const sql = require("mssql");
const conection = require("../BD/database");

const findAll = () => {
  return new Promise((resolve, reject) => {
    conection.obtenerPool().then((pool) => {
        return pool
          .request()
          .query(
            "SELECT [CamaraId],[CamaraNo],[CamaraDescripcion],[CamaraLat],[CamaraLong],[DeviceId] FROM [BDCope].[dbo].[VVIGET_CAMARAS]"
          );
      }).then((result) => {
        resolve(result);
        conection.cerrarPool();
      }).catch((err) => {
        reject(err);
      });
  });
};

const findId = (id) => {
  return new Promise((resolve, reject) => {
    conection.obtenerPool().then((pool) => {
      return pool.request().input(
        "CamaraId", sql.Int, id).query("SELECT [CamaraId],[CamaraDescripcion],[CamaraLat],[CamaraLong],[DeviceId] FROM [BDCope].[dbo].[VVIGET_CAMARAS] WHERE CamaraId =@CamaraId");
      }).then((result) => {
        resolve(result);
        conection.cerrarPool();
      }).catch((err) => {
        reject(err);
      });
  });
}
module.exports = {
  findAll,
  findId
}