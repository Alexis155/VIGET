const sql = require("mssql");
const conection = require("../BD/database");

const findAll = () => {
  return new Promise((resolove, reject) => {
    conection
      .obtenerPool()
      .then((pool) => {
        return pool.request().query("SELECT * FROM [BDCope].[dbo].[VVIGET_GPOSOPER] ORDER BY CveGpoOper ASC");
      }).then((result) => {
        resolove(result);
        conection.cerrarPool();
      }).catch((err) => {
        reject(err);
      });
  });
};

module.exports = {
  findAll
};