const sql = require('mssql');
const connection = require('../BD/database');

const Unidades = () => {
	return new Promise((resolve, reject) => {
		connection
			.PoolKapok()
			.then((pool) => {
				return pool
					.request()
					.query(
						'SELECT [terid],[gpstime],[altitude],[gpslat],[gpslng],[region],[speed],[recordspeed],[state],[CveVehi] FROM [GpsKapok].[dbo].[KapokActual]'
					);
			})
			.then((result) => {
				resolve(result);
				connection.cerrarKapok();
			})
			.catch((err) => {
				reject(err);
			});
	});
};

const Bloqueo = (CveVehi) => {
	return new Promise((resolve, reject) => {
		connection
			.PoolKapok()
			.then((pool) => {
				return pool
					.request()
					.input('CveVehi', sql.VarChar, CveVehi)
					.query(
						'SELECT * FROM [GpsKapok].[dbo].[BloqueoVideoUnidades] WHERE CveVehi = @CveVehi'
					);
			})
			.then((result) => {
				console.log('Consulta de bloqueo exitosa, resultado: ', result);
				resolve(result);
				connection.cerrarKapok();
			})
			.catch((err) => {
				console.log('Consulta de bloqueo fallida, error: ', err);
				reject(err);
			});
	});
};

const historico = (CveVehi, fechaI, fechaF) => {
	console.log(fechaI);
	return new Promise((resolve, reject) => {
		connection
			.PoolKapok()
			.then((pool) => {
				return pool
					.request()
					.input('CveVehi', sql.VarChar, CveVehi)
					.input('fechaI', sql.VarChar, fechaI)
					.input('fechaF', sql.VarChar, fechaF)
					.query(
						'SELECT * FROM [GpsKapok].[dbo].[KapokHistorico] where CveVehi = @CveVehi AND FechayHoraGPS > @fechaI AND FechayHoraGPS <= @fechaF'
					);
			})
			.then((result) => {
				resolve(result);
				connection.cerrarKapok();
			})
			.catch((err) => {
				reject(err);
			});
	});
};

module.exports = {
	Unidades,
	Bloqueo,
	historico,
};
