// const url = "http://10.17.110.21:1025"; http://10.17.110.67:1025
const user = "desarrollo";
const password = "1fy0uw1shyouc@n";
var section = null;
var cams = new Array();
var Id = null;
var attachContainerClick = true;
window.addEventListener('load', function () {
    conectar(window.location.href);
    init()
});

function conectar(location) {
    let url = "";
    if (location.search("https://viget.sspmqro.gob.mx") != -1) {
        url = "https://189.254.220.184:1026"
       // url = "https://sspmqro.gob.mx/camaras"
    } else {
        url = "http://10.17.110.67:1025"
        //url = "http://10.17.110.21:1025"
        // url = "https://10.17.110.26:1026" 
    }
    var params = {
        connectionDidLogIn: connectionDidLogIn,
        connectionFailedToLogIn: connectionFailedToLogIn
      };
      XPMobileSDKSettings.MobileServerURL = url;
      XPMobileSDK.addObserver(params);
  
      XPMobileSDK.connect(url);
}

function init() {
    section = document.querySelector(".camera");
    section.addEventListener("click", connect);
}

function connect() {
    $("#ModalLoader").modal({backdrop: "static"});
    XPMobileSDK.login(user, password, {
        SupportsAudioIn: 'Yes',
        SupportsAudioOut: 'Yes'
    }); 
};

var connectionFailedToLogIn = function (error) {
    $("#ModalLoader").modal('hide');
    Swal.fire({
        icon: 'error',
        title: '¡Error!',
        text: 'Por favor reintente mas tarde',
        confirmButtonColor: '#DD6B55',
        confirmButtonText:'<href="/mapa">Regresar</href=>',
      })
}

var connectionDidLogIn = function () {
    Id = $("#DeviceId").val();
    $("#ModalLoader").modal('hide');
    section.removeEventListener("click", connect)
    var canvas = document.querySelector("#canvas");
    var canvasContext = canvas.getContext('2d');
    var video = document.querySelector("#video");
    var image = document.createElement('img');
    image.addEventListener('load', onImageLoad);
    image.addEventListener('error', onImageError);
    var imageURL, videoController;
    var drawing = false;

    var videoConnectionObserver = {
        videoConnectionReceivedFrame: videoConnectionReceivedFrame
    }

    XPMobileSDK.library.Connection.webSocketBrowser = false;
    /**
     * Requesting a video stream. 
     */
    var streamRequest = XPMobileSDK.RequestStream(RequestStreamParams(Id, 'Live'), requestStreamCallback, function (error) { } );
    
    function requestStreamCallback(videoConnection) {
        videoController = videoConnection;
        videoConnection.addObserver(videoConnectionObserver);
        videoConnection.open();
        var event = new CustomEvent('playStream', {
            detail: {
                cameraId: Id,
                videoConnection: videoConnection
            }
        });

        container.parentNode.dispatchEvent(event);
    }

    document.getElementById('container').classList.add('playing', 'live');
    if (attachContainerClick) {
        document.getElementById('container').addEventListener('click', switchToPlayback);
    }

    document.querySelector('.bottombar').setAttribute("style", "display:none;");
    document.querySelector('.bottombar').classList.remove("onPause");
    document.querySelector('.playBackButton').addEventListener('click', playBackwardTrigger);
    document.querySelector('.playForwardButton').addEventListener('click', playForwardTrigger);

    var playbackTimestamp = new Date();
    var playbackSpeed = 0;

    var isLive = true;

    /**
     * Executed on received frame. 
     */
    function videoConnectionReceivedFrame(frame) {
        if (!drawing && frame.dataSize > 0) {

            drawing = true;

            if (frame.hasSizeInformation) {
                var multiplier = (frame.sizeInfo.destinationSize.resampling * XPMobileSDK.getResamplingFactor()) || 1;
                image.width = multiplier * frame.sizeInfo.destinationSize.width;
                image.height = multiplier * frame.sizeInfo.destinationSize.height;
            }

            if (imageURL) {
                window.URL.revokeObjectURL(imageURL);
            }

            imageURL = window.URL.createObjectURL(frame.blob);

            image.src = imageURL

            if (!isLive && frame.timestamp.getTime() != playbackTimestamp.getTime())
            {
                    updateTime(frame.timestamp);
            }
        }
    }

    function onImageLoad(event) {
        canvas.width = image.width;
        canvas.height = image.height;
        canvasContext.drawImage(image, 0, 0, canvas.width, canvas.height);

        drawing = false;
    }

    function onImageError(event) {
        console.log("valio madre")
        drawing = false;
    }

     /**
     * Stop camera stream 
     */
    function stop() {

        if (videoController) {
            videoController.removeObserver(videoConnectionObserver);
            videoController.close();
            videoController = null;
        }

        if (streamRequest) {
            XPMobileSDK.cancelRequest(streamRequest);
            streamRequest = null;
        }

        document.getElementById('container').removeEventListener('click', stop);
    };

    function resetState() {

        playbackSpeed = 0;
        updatePlaybackButtons();

        if (streamRequest) {
            XPMobileSDK.cancelRequest(streamRequest);
            streamRequest = null;
        }
    }
     /**
     * Switch to camera playback mode. 
     */
    function switchToPlayback() {

        if (!isLive) return;

        isLive = false;

        stop();

        document.getElementById('container').removeEventListener('click', switchToPlayback);
        document.getElementById('container').classList.remove('live');
        
        playbackTimestamp = new Date();

        showPlaybackControls();
        resetState();

        updateTime(playbackTimestamp);

        streamRequest = XPMobileSDK.RequestStream(RequestStreamParams(Id, 'Playback'), requestStreamCallback, null);
    }

     /**
     * Switch camera to live video
     */
    function switchToLive(e) {
        e.stopPropagation();
        e.preventDefault();

        if (isLive) return;

        isLive = true;

        stop();

        document.getElementById('container').removeEventListener('click', switchToLive);
        document.getElementById('container').addEventListener('click', switchToPlayback);
        document.getElementById('container').classList.add('playing', 'live');
        document.querySelector('.bottombar').setAttribute("style", "display:none;");
        document.querySelector('.bottombar').classList.remove("onPause");

        resetState();
        
        streamRequest = XPMobileSDK.RequestStream(RequestStreamParams(Id, 'Live'), requestStreamCallback, function (error) { } );
    }

      /**
     * Trigger camera play backwards
     */
    function playBackwardTrigger() {
        if (playbackSpeed < 0) {
            playbackChangeSpeed(0);
        }
        else {
            playbackChangeSpeed(-1);
        }
    }

     /**
     * Trigger camera play forward
     */
    function playForwardTrigger() {
        if (playbackSpeed > 0) {
            playbackChangeSpeed(0);
        }
        else {
            playbackChangeSpeed(1);
        }
    }

     /**
     * Show camera playback controls
     */
    function showPlaybackControls() {
        document.getElementById('container').removeEventListener('click', switchToPlayback);
        document.getElementById('container').classList.remove('playing');

        document.querySelector('.bottombar').setAttribute("style", "display:block;");
        document.querySelector('.bottombar').classList.add("onPause");
        
        document.querySelector('.pauseButton').setAttribute('title', 'Live');
        document.querySelector('.pauseButton').addEventListener('click', switchToLive);
    }

    /**
     * Change video speed
     */
    function playbackChangeSpeed(speed) {
        if (!videoController || speed == playbackSpeed) return;

        speed = Math.round(speed);
        
        var params = {
            VideoId: videoController.videoId,
            Speed: speed
        };

        XPMobileSDK.ChangeStream(params, function () {
            var eventType = speed === 0 ? 'pauseStream' : 'playStream';
            var event = new CustomEvent(eventType, {
                detail: {
                    videoConnection: videoController
                }
            });

            container.parentNode.dispatchEvent(event);
        });

        if (speed == 0) {
            playbackSpeed = 0;
        }
        else if (speed < 0) {
            playbackSpeed = -1;
        }
        else if (speed > 0) {
            playbackSpeed = 1;
        }

        updatePlaybackButtons();
    }

      /**
     * Update playback controls depending on the video speed
     */
    function updatePlaybackButtons() {
        var playForwardButton = document.querySelector('.playForwardButton');
        var playBackButton = document.querySelector('.playBackButton');

        if (playbackSpeed == 0) {
            playForwardButton.classList.remove('active');
            playForwardButton.title = "Play forward";
            playBackButton.classList.remove('active');
            playBackButton.title = "Play backwards";
        }
        else if (playbackSpeed < 0) {
            playForwardButton.classList.remove('active');
            playForwardButton.title = "Play forward";
            playBackButton.classList.add('active');
            playBackButton.title = "Pause";
        }
        else if (playbackSpeed > 0) {
            playForwardButton.classList.add('active');
            playForwardButton.title = "Pause";
            playBackButton.classList.remove('active');
            playBackButton.title = "Play backwards";
            document.getElementById('container').classList.add('playing');
        }
    }

     /**
     * Updates time element
     */
    function updateTime(timestamp) {

        if (isLive) return;

        playbackTimestamp = timestamp;

        var date = new Date(timestamp);

        var hours = date.getHours();
        var minutes = "0" + date.getMinutes();
        var seconds = "0" + date.getSeconds();
        var formattedTime = hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);

        document.querySelector('.playTimeIndex').innerHTML = formattedTime;
    }
};

function RequestStreamParams(cameraId, signalType) {
    return {
        CameraId: cameraId,
        DestWidth: 500,
        DestHeight: 400,
        SignalType: signalType /*'Live' or 'Playback'*/,
        MethodType: 'Push' /*'Pull'*/,
        Fps: 25, // This doesn't work for Pull mode, but we have to supply it anyway to keep the server happy
        ComprLevel: 71,
        KeyFramesOnly: 'No' /*'Yes'*/, // Server will give only key frame thumb nails. This will reduce FPS
        RequestSize: 'Yes',
        StreamType: 'Transcoded'
    };
}
