$('#option').change(() => {
  if ($('#option').val() == 'tabletas') {
    $('#BuscadorUnidad').addClass('quitar');
    $('#listaSider').addClass('quitar');
    $('#listaTablets').removeClass('quitar');
    $('#BuscadorTableta').removeClass('quitar');
    listaTabletas();
  } if ($('#option').val() == 'unidades') {
    $('#BuscadorUnidad').removeClass('quitar');
    $('#listaSider').removeClass('quitar');
    $('#listaTablets').addClass('quitar');
    $('#BuscadorTableta').addClass('quitar');
  }
});

$("#buscarT").on("keyup", function () {
  var patron = $(this).val();

  // si el campo está vacío
  if (patron == "") {
    // mostrar todos los elementos
    $(".listaTablet").css("display", "list-item");

    // si tiene valores, realizar la búsqueda
  } else {
    // atravesar la listaTablet
    $(".listaTablet").each(function () {
      if ($(this).text().indexOf(patron) < 0) {
        // si el texto NO contiene el patrón de búsqueda, esconde el elemento
        $(this).css("display", "none");
      } else {
        // si el texto SÍ contiene el patrón de búsqueda, muestra el elemento
        $(this).css("display", "list-item");
      }
    });
  }
});

