var info = null;
var idUnidad = null;
var flvPlayer = null;
var user = null;
var pass = null;
var unidad = null;
window.addEventListener('load', function () {
	idUnidad = $('#idUnidad').val();
	user = $('#user').val();
	pass = $('#pass').val();
	let CveVehi = $('#CveVehi').val();
	var parrafo = document.getElementById('unidad');
	unidad = parrafo.innerHTML;
	console.log(CveVehi);
	url(1);
});

async function registro(CveUsuario, VideoInicio, VideoFin, CveVehi, error) {
	try {
		if (VideoInicio) {
			await fetch('/insert', {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
				},
				body: JSON.stringify({
					CveUsuario: CveUsuario,
					CveVehi: CveVehi,
				}),
			});
		} else if (VideoFin) {
			await fetch('/update', {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
				},
				body: JSON.stringify({
					CveUsuario: CveUsuario,
					error: error,
				}),
			});
		}
	} catch (error) {
		console.log('Error: ', error);
	}
}
async function url(canal) {
	try {
		const response = await fetch('/video/url', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({
				canal: canal,
				terId: idUnidad,
				estatus: window.location.href,
				user: user,
				pass: pass,
			}),
		});
		const data = await response.json();
		if (data.error) {
			error();
		} else {
			video(data.video);
		}
	} catch (error) {
		console.log('error', error);
	}
}

function video(url) {
	var video = document.getElementById('video');
	var height = $(window).height() - 200;
	video.style.cssText = 'width:100%; height:' + height + 'px';
	if (flvjs.isSupported()) {
		var videoElement = document.getElementById('video');
		flvPlayer = flvjs.createPlayer({
			type: 'flv',
			isLive: true, // seems not necessary
			url: url,
		});
		flvPlayer.attachMediaElement(videoElement);
		flvPlayer.load();
		flvPlayer.on(flvjs.Events.ERROR, (err) => error());
		flvPlayer.play();
		registro(user, true, null, unidad, null);
		setTimeout(cerrar, 300000);
	}
}
function error() {
	Swal.fire({
		icon: 'error',
		title: '¡Error!',
		text: 'Por favor reintente mas tarde',
		confirmButtonColor: '#f53b3b',
		confirmButtonText:
			'<button onclick="cerrarError()" class="btn etiqueta-abordo">Cerrar</button>',
	});
}
function cerrar() {
	flvPlayer.pause();
	flvPlayer.detachMediaElement();
	flvPlayer.destroy();
	flvPlayer = null;
	registro(user, null, true, unidad, null);
	window.close();
}
function cerrarError() {
	flvPlayer.pause();
	flvPlayer.detachMediaElement();
	flvPlayer.destroy();
	flvPlayer = null;
	registro(user, null, true, unidad, true);
	window.close();
}
var header = document.getElementById('canales');
var btns = header.getElementsByClassName('btn');
for (var i = 0; i < btns.length; i++) {
	btns[i].addEventListener('click', function () {
		var current = document.getElementsByClassName('active');
		current[0].className = current[0].className.replace(' active', '');
		this.className += ' active';
		if (flvPlayer.play()) {
			flvPlayer.destroy();
			url(this.innerHTML);
		}
	});
}
