
$(document).ready(function () {
  animacionLoader();
  $(".select-personal").select2({
    width: "100%",
  });
});

function animacionLoader() {
  let loader = document.getElementById("loader");
  let activos = document.getElementById("table-body");
  let inactivos = document.getElementById("table-body-0");
  setTimeout(function () {
    (loader.style.display = "none"),
      (activos.innerHTML = ""),
      (inactivos.innerHTML = ""),
      obtenerAsignacionPasadas();
  }, 500);
}

function obtenerAsignacionPasadas() {
  let cveVehi = document.getElementById("cveVehi").innerText;
  let cuerpoTabla = document.getElementById("table-body");
  if (cveVehi) {
    fetch("/obtenerAsignaciones", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ cveVehi }),
    })
      .then((res) => {
        res.json().then((data) => {
          if (data.asignaciones.length > 0) {
            llenarTabla(data.asignaciones);
          } else {
            cuerpoTabla.innerHTML =
              '<tr><td colspan ="5"> <h2>' +
              "Sin registros previos" +
              " </h2> </td></tr>";
          }
        });
      })
      .catch((err) => {
        console.log(err);
        Swal.fire({
          title: "SISCOP",
          text: "¡Algo salió mal, vuelve a intentarlo!",
          icon: "warning",
          confirmButtonText: "Aceptar",
        });
      });
  } 
}

function llenarTabla(datos) {
  let bodyTable1 = document.getElementById("table-body");
  let bodyTable0 = document.getElementById("table-body-0");
  let registrosActivos = "";
  let registrosPasados = "";

  const rol = $("#tabla-asignacion").data("rol");

  let contadorAsignacionesPasadas = 0;
  for (let u of datos) {
    if (u.estatus == 1) {
      registrosActivos += '<tr class="tr_rojo">';
      registrosActivos += '<td><a href="/agregar" class="btn etiqueta-abordo disabled" aria-disabled="true"><i class="fa fa-user-plus"></i></a></td>';
      registrosActivos += "<td><a>" + u.nombre + "</a></td>";
      if (rol == "rgion") {
        registrosActivos +='<td>🚔<a href="#" class="badge etiqueta-abordo" >En unidad</a></td>';
        registrosActivos +='<td><a href="#" class="btn btn-amarillo"  onclick="desasignar(' +u.consecutivo +"," +u.ano_asignacion +')"><i class="fa fa-user-times" data-toggle="tooltip" title="Bajar de la unidad" ></i></a></td> </tr>';
      } else {
        registrosActivos +='<td>🚔<a href="#" class="badge etiqueta-abordo" >En unidad</a></td>';
        registrosActivos +='<td><a href="#" class="btn btn-amarillo"><i class="fa fa-user-times" data-toggle="tooltip" title="Bajar de la unidad" ></i></a></td> </tr>';
      }
    } else {
      contadorAsignacionesPasadas += 1;
      if (contadorAsignacionesPasadas <= 5) {
        registrosPasados += '<tr class="tr_verde">';
        if (rol == "rgion") {
          registrosPasados +=
            '<td><a href="#" class="btn etiqueta-abordo"  onclick="crearAsignacion(' +u.CveNumEmp +')"><i class="fa fa-user-plus" data-toggle="tooltip" data-placement="top" title="Subir a esta unidad"></i></a></td>';
          registrosPasados += "<td>" + u.nombre + "</td>";
          registrosPasados +='<td>‍<a href="#" class="badge etiqueta-anterior" >Asignado anteriormente</a></td>';
          registrosPasados +='<td><a href="#" class="btn btn-amarillo disabled" aria-disabled="true"><i class="fa fa-user-times"></i></a></td> </tr>';
        } else {
          registrosPasados +='<td><a href="#" class="btn etiqueta-abordo"><i class="fa fa-user-plus" data-toggle="tooltip" data-placement="top" title="Subir a esta unidad"></i></a></td>';
          registrosPasados += "<td>" + u.nombre + "</td>";
          registrosPasados +='<td>‍<a href="#" class="badge etiqueta-anterior" >Asignado anteriormente</a></td>';
          registrosPasados += '<td><a href="#" class="btn btn-amarillo disabled" aria-disabled="true"><i class="fa fa-user-times"></i></a></td> </tr>';
        }
      }
    }
  }
  bodyTable1.innerHTML = registrosActivos;
  bodyTable0.innerHTML = registrosPasados;
}

document.getElementById("btnAddPerson").onclick = function (params) {
  crearAsignacion();
};

function desasignar(consecutivo, anoAsignacion) {
  if (consecutivo && anoAsignacion) {
    fetch("/desasginarOficial", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ consecutivo, anoAsignacion }),
    }).then((response) => response.json()).then((data) => {
        Swal.fire({
          position: "top-end",
          icon: "success",
          title: data.mensaje,
          showConfirmButton: false,
          timer: 1500,
        });
        animacionLoader();
      }).catch((err) => {
        Swal.fire({
          title: "SISCOP",
          text: "¡Algo salió mal, vuelve a intentarlo!",
          icon: "warning",
          confirmButtonText: "Aceptar",
        });
      });
  }
}

async function crearAsignacion(cveNumEmp) {
  let cveEmp = "";
  let cveEmpSelect = document.getElementById("selectPersonas").value;
  let cveVehi = document.getElementById("cveVehi").innerText;
  let CveAsigna = document.getElementById("CveAsigna").value;
  if (cveNumEmp) {
    cveEmp = cveNumEmp;
  } else {
    cveEmp = cveEmpSelect;
  }
  if (cveEmp && cveVehi) {
    fetch("/agregar", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ cveEmp: cveEmp, cveVehi, CveAsigna: CveAsigna}),
    }).then((response) => response.json()).then((data) => {
        // obtenerAsignacionPasadas();
        switch (data.estatus) {
          case "ocupado":
            Swal.fire({
              title: "¡Precaución!",
              text: data.mensaje + " ¿Quieres reasignar a esta unidad?",
              icon: "warning",
              showCancelButton: true,
              confirmButtonColor: "#3085d6",
              cancelButtonColor: "#d33",
              confirmButtonText: "¡Si, reasignar a esta unidad!",
            }).then((result) => {
              if (result.value) {
                reasignar(data.consecutivo, data.ano, cveEmp);
              }
            });
            break;
          case "libre":
            Swal.fire({
              position: "top-end",
              icon: "success",
              title: data.mensaje,
              showConfirmButton: false,
              timer: 1500,
            });
            animacionLoader();
            break;
          default:
            Swal.fire({
              title: "SISCOP",
              text: "¡" + data.mensaje + "!",
              icon: "warning",
              confirmButtonText: "Aceptar",
            });
            break;
        }
      })
      .catch((err) => {
        Swal.fire({
          title: "SISCOP",
          text: "¡Algo salió mal, vuelve a intentarlo!",
          icon: "warning",
          confirmButtonText: "Aceptar",
        });
      });
  } else {
  }
}

function reasignar(consecutivo, anoAsignacion, cveNumEmp) {
  let cveVehi = document.getElementById("cveVehi").innerText;
  let CveAsigna = document.getElementById("CveAsigna").value;
  if (consecutivo && anoAsignacion) {
    fetch("/reAsginarOficial", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ consecutivo, anoAsignacion, cveVehi, cveNumEmp, CveAsigna }),
    }).then((response) => response.json()).then((data) => {
        Swal.fire({
          position: "top-end",
          icon: "success",
          title: data.mensaje,
          showConfirmButton: false,
          timer: 1500,
        });
        animacionLoader();
      }).catch((err) => {
        Swal.fire({
          title: "SISCOP",
          text: "¡Algo salió mal, vuelve a intentarlo!",
          icon: "warning",
          confirmButtonText: "Aceptar",
        });
      });
  }
}