const map = L.map('map').setView([20.6275523, -100.4091127], 16);

L.tileLayer(
	'https://api.mapbox.com/styles/v1/kevin155/ckl6qaygo19i917nm3vp6b66k/tiles/256/{z}/{x}/{y}@2x?access_token=pk.eyJ1Ijoia2V2aW4xNTUiLCJhIjoiY2tsNnEyb29hMWtxbjJubW02NzRiN2c0biJ9.5v9L-brCDD9YxULSBEouSA',
	{
		minZoom: 3,
		tileSize: 512,
		zoomOffset: -1,
		maxZoom: 19,
		attribution: 'SSPMQ© DIR. INF. DEP. SISTEMAS',
	}
).addTo(map);

let unidadMarker = L.icon({
	iconUrl: '/images/flash.gif',
	iconSize: [60, 60],
	iconAnchor: [30, 30],
});

$(document).ready(function () {
	$('.js-example-basic-single').select2({
		width: 'resolve',
	})

	$('#buscar').click(function () {
		let _unidades = $('#unidad').val()
		let _fechaI = $('#fechaI').val()
		let _fechaF = $('#fechaF').val()
		_fechaI = _fechaI.substr(0, 16).replace('T', ' ')
		_fechaF = _fechaF.substr(0, 16).replace('T', ' ')

		if ( _unidades == 'Ninguno' || _fechaI == '' || _fechaF == '' ) {
			Swal.fire({
				icon: 'warning',
				title: '¡Advertencia!',
				text: 'Verifique que todos los datos sean correctos',
				confirmButtonColor: '#6c757d',
				confirmButtonText: 'Verificar',
			})
		} else {
			$.ajax({
				url: '/historicoU',
				type: 'POST',
				dataType: 'json',
				data: {
					unidades: _unidades,
					fechaI: _fechaI + ':00',
					fechaF: _fechaF + ':59',
				},
				success: function (data) {
                    console.log("Qty query", data);
                    if (data.length > 0 ) {
                        let estaticos = [], iter_temp = '', arrayTemp = [], resultados = [], x, d, items;

					    for (let i = 0; i < data.length; i++) {
					    	if (iter_temp.gpslat + ' ' + iter_temp.gpslng ==data[i].gpslat + ' ' + data[i].gpslng) {
					    		arrayTemp.push(iter_temp)
					    	} else {
					    		if (arrayTemp.length > 0) {
					    			arrayTemp.push(iter_temp)
					    			estaticos.push(arrayTemp)
					    			arrayTemp = []
					    		}
					    	}

					    	iter_temp = data[i]

					    	if (data.length == i + 1) {
					    		if (arrayTemp.length > 0) {
					    			arrayTemp.push(iter_temp)
					    			estaticos.push(arrayTemp)
					    			arrayTemp = []
					    		}
					    	}
					    }

					    for (let i = 0; i < estaticos.length; i++) {
					    	x = {}
					    	d = diferenciaFechas(
					    		estaticos[i][0].gpstime,
					    		estaticos[i][estaticos[i].length - 1].gpstime
					    	)
					    	x = Object.assign(x, {
					    		id: i,
                                unidad: _unidades,
					    		f1: estaticos[i][0].gpstime,
					    		f2: estaticos[i][estaticos[i].length - 1].gpstime,
					    		tiempo: d,
					    		coords: {
					    			lat: estaticos[i][0].gpslat,
					    			lon: estaticos[i][0].gpslng,
					    		},
					    	})
					    	resultados.push(x)
					    }

                        let unidad_number = $('.js-example-basic-single').select2('data')
                        unidad_number = unidad_number[0].text.trim();

                        let time = {};
                        let f1, f2, timestr;
                        for (let i = 0; i < resultados.length; i++) {
                            time = resultados[i].tiempo;
                            if (time.min > 5 || time.hora > 0) {
                                f1 = resultados[i].f1;
                                f1 = f1.toString();
                                f2 = resultados[i].f2;
                                f2 = f2.toString();
                                timestr = time.hora+ "hrs " + time.min + "min";
                                items += "<tr><th scope='row'>" +(i+1) +"</th>"
                                + "<td>"+ unidad_number +"</td>"
                                + "<td>"+f1+"</td>"
                                + "<td>"+f2+"</td>"
                                + "<td>"+timestr+ "</td>"
                                + "<td>"
                                + "<button type='button' class='btn btn-outline-success btn-light my-sm-0 bt-search bt-mb-resp'"
                                +`onclick="locateUnidad(${resultados[i].coords.lat},${resultados[i].coords.lon}, '${f1}', '${f2}', '${timestr}' )" >`
                                + "<i class='fas fa-crosshairs'></i>"
                                + "</button>"
                                + "</td>"
                                +"</tr>";    
                            }
                        };

                        $("#resultados").empty();
                        $('#resultados').append(items);
                        items = '';
                    }else {
                        Swal.fire({
                            icon: 'info',
                            title: '¡No se encontraron datos!',
                            text: 'No se obtuvó registros de GPS de la unidad',
                            confirmButtonColor: '#007bff',
                            confirmButtonText: 'Aceptar',
                        })
                    }
				},
				fail: function (e) {
					console.log('Buscar function failed, error', e)
				},
			})
		}
	})
})

function diferenciaFechas(fechaIni, fechaFin) {
	let desde = new Date(fechaIni), 
        hasta = new Date(fechaFin), 
        diff = new Date(hasta.getTime() - desde.getTime());
        let res = {};

    res['año'] = diff.getFullYear() - 1969,
	res['mes'] = ('00' + (diff.getMonth() + 1)).slice(-2) - 12,
	res['dia'] = ('00' + diff.getDate()).slice(-2) - 31,
	res['hora'] = ('00' + diff.getHours()).slice(-2) - 18,
	res['min'] = ('00' + diff.getMinutes()).slice(-2) - 00,
	res['sec'] = ('00' + diff.getSeconds()).slice(-2) - 00
	console.log(res['año'],res['mes'],res['dia'],res['hora'],res['min'],res['sec']);
    return res
}

let marker;

function locateUnidad(lati, long, start, end, time) {
    console.log(lati, long);
    $('#modal-mapa').modal('show');
    setTimeout(function() {
        map.invalidateSize();
    }, 10);
    map.setView([lati, long], 17);
    if (marker) {
        map.removeLayer(marker)
    }
    marker = L.marker([lati, long], {icon: unidadMarker}).bindPopup('Posición de la unidad');
    map.addLayer(marker);
    let unidad_number = $('.js-example-basic-single').select2('data')
    unidad_number = unidad_number[0].text.trim();
    $("#modal-unidad").empty();
    $('#modal-unidad').append('<b>'+ unidad_number +'</b>');
    $("#data-unidad").empty();
    $('#data-unidad').append('<br>Tiempo ' + time + ' del ' + start + ' al ' + end);
}
