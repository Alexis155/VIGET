var layerCamara = null;
var layers = null;
var alertas;
var region = null;
var tabletaMarkers;
var lista = new Array();
var patrullaMarkers;
var tabM = new Array();
var tabletas = new Array();
var patrullas = new Array();
var polygonos = new Array();
var emergencias = new Array();
var camaraMarkers = new Array();
var deferred;
var marcadores = {
	unidades: 'unidades',
	tabletas: null,
	camaras: null,
	emergencias: 'emergencias',
};
const iconCamera = L.icon({
	iconUrl: '/images/unnamed.png',
	iconSize: [23, 23],
	iconAnchor: [10, 20],
});
const iconAlertRed = L.icon({
	iconUrl: '/images/mapaAlertaRojo.gif',
	iconSize: [23, 23],
	iconAnchor: [10, 20],
});
const iconCar = L.icon({
	iconUrl: '/images/police-car-32.png',
});
const iconEalta = L.icon({
	iconUrl: '/images/alta.gif',
	iconSize: [30, 30],
	iconAnchor: [15, 25],
});
const iconEmedia = L.icon({
	iconUrl: '/images/media.gif',
	iconSize: [45, 45],
	iconAnchor: [15, 25],
});
const iconEbaja = L.icon({
	iconUrl: '/images/baja.gif',
	iconSize: [45, 45],
	iconAnchor: [15, 25],
});
const iconCarFail = L.icon({
	iconUrl: '/images/PoliceFail.ico',
	// iconSize: [25,25],
	// iconAnchor: [10,20]
});
const iconTablet = L.icon({
	iconUrl: '/images/tablet-24.png',
	iconSize: [15, 15],
	iconAnchor: [10, 20],
});
const basic = L.tileLayer(
		'https://api.mapbox.com/styles/v1/kevin155/ckl6rbs691ahs17nm4kei92yb/tiles/256/{z}/{x}/{y}@2x?access_token=pk.eyJ1Ijoia2V2aW4xNTUiLCJhIjoiY2tsNnEyb29hMWtxbjJubW02NzRiN2c0biJ9.5v9L-brCDD9YxULSBEouSA',
		{
			minZoom: 3,
			tileSize: 512,
			zoomOffset: -1,
			attribution: 'SSPMQ DIR. INF. DEP. SISTEMAS',
		}
	),
	mapBox = L.tileLayer(
		'https://api.mapbox.com/styles/v1/kevin155/ckl6qaygo19i917nm3vp6b66k/tiles/256/{z}/{x}/{y}@2x?access_token=pk.eyJ1Ijoia2V2aW4xNTUiLCJhIjoiY2tsNnEyb29hMWtxbjJubW02NzRiN2c0biJ9.5v9L-brCDD9YxULSBEouSA',
		{
			minZoom: 3,
			tileSize: 512,
			zoomOffset: -1,
			attribution: 'SSPMQ DIR. INF. DEP. SISTEMAS',
		}
	),
	mapSatelital = L.tileLayer(
		'https://api.mapbox.com/styles/v1/kevin155/ckl6qg3rw0kof17ovw5r9s5v9/tiles/256/{z}/{x}/{y}@2x?access_token=pk.eyJ1Ijoia2V2aW4xNTUiLCJhIjoiY2tsNnEyb29hMWtxbjJubW02NzRiN2c0biJ9.5v9L-brCDD9YxULSBEouSA',
		{
			minZoom: 3,
			tileSize: 512,
			zoomOffset: -1,
			attribution: 'SSPMQ DIR. INF. DEP. SISTEMAS',
		}
	);
var mymap = L.map('qroMap', {
	center: [20.58899, -100.390893],
	zoom: 15,
	scrollWheelZoom: true,
	layers: [basic],
});
var baseMaps = {
	Basico: basic,
	Calles: mapBox,
	Satelidtal: mapSatelital,
};
L.control.layers(baseMaps).addTo(mymap);
function countAlerts() {
	try {
		if (alertas.length > 0) {
			for (let i = 0; alertas.length > i; i++) {
				let marcador = patrullas.filter(
					(ele) => ele.unidad == alertas[i].CveVehi
				);
				let ic = L.divIcon({
					className: 'custom-div-icon',
					html:
						"<div class='marker-pin'>" +
						alertas[i].CveVehi +
						"</div><img src='/images/AlertIcon.ico' class='img-fluid'/>",
					iconSize: [50, 62],
					iconAnchor: [30, 62],
				});
				marcador[0].marcador.setIcon(ic);
			}
			const Toast = Swal.mixin({
				toast: true,
				position: 'top',
				showConfirmButton: true,
				timer: 15000,
				timerProgressBar: true,
				showCancelButton: true,
			});
			Toast.fire({
				icon: 'warning',
				title: 'Hay ' + alertas.length + ' alertas',
				confirmButtonText: 'Ver',
				cancelButtonText: 'Cancelar',
			}).then((result) => {
				if (result.isConfirmed) {
					window.open('alertas');
				}
			});
		}
	} catch (error) {
		console.log('Error', error);
	}
}
$(document).ready(function () {
	deferred = $.Deferred();
	marker();
	obtenerLimitesRegiones()
		.finally(() => {
			obtenerPatrulla()
				.finally(() => {
					obtnerEmergencias().finally(() => {
						listaUnidades().finally(() => {
							obtenerCamaras().finally(() => {
								obtenerTraccar();
								setTimeout(consulta(), 30000);
							});
						});
					});
				})
				.catch((e) => {
					console.log('err', e);
				});
		})
		.catch(() => {
			console.log('error');
		});

	function consulta() {
		obtenerPatrulla().finally(() => {
			obtnerEmergencias().finally(() => {
				listaUnidades().finally(() => {
					obtenerTraccar();
				});
			});
		});
		setTimeout(consulta, 30000);
	}
	$('#regiones').change(() => {
		region = $('#regiones').val();
		obtenerPatrulla().then(() => {
			listaUnidades().then(() => {
				agragarMarcadores();
			});
		});
	});
	$('#unidades').change(() => {
		if ($('#unidades').prop('checked') != true) {
			marcadores.unidades = null;
			agragarMarcadores();
		} else {
			marcadores.unidades = 'unidades';
			agragarMarcadores();
		}
	});
	$('#tabletas').change(() => {
		if ($('#tabletas').prop('checked') != true) {
			marcadores.tabletas = null;
			agragarMarcadores();
		} else {
			marcadores.tabletas = 'tabletas';
			agragarMarcadores();
		}
	});
	$('#cams').change(() => {
		if ($('#cams').prop('checked') != true) {
			marcadores.camaras = null;
			agragarMarcadores();
		} else {
			marcadores.camaras = 'camaras';
			agragarMarcadores();
		}
	});
	$('#emergencias').change(() => {
		if ($('#emergencias').prop('checked') != true) {
			marcadores.emergencias = null;
			agragarMarcadores();
		} else {
			marcadores.emergencias = 'emergencias';
			agragarMarcadores();
		}
	});
	// Buscador
	$('#buscar').on('keyup', function () {
		var patron = $(this).val();

		// si el campo está vacío
		if (patron == '') {
			// mostrar todos los elementos
			$('.lista').css('display', 'list-item');
			$('.listRegion').css('display', 'list-item');
			// si tiene valores, realizar la búsqueda
		} else {
			// atravesar la lista
			$('.lista').each(function () {
				if ($(this).text().indexOf(patron) < 0) {
					// si el texto NO contiene el patrón de búsqueda, esconde el elemento
					$(this).css('display', 'none');
					$('.listRegion').css('display', 'none');
				} else {
					// si el texto SÍ contiene el patrón de búsqueda, muestra el elemento
					$(this).css('display', 'list-item');
					$('.listRegion').css('display', 'list-item');
				}
			});
		}
	});
});

async function obtnerEmergencias() {
	try {
		let response = await fetch('/emergencias', {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json',
			},
		});
		let data = await response.json();
		emergencias = data;
		agragarMarcadores();
	} catch (error) {
		console.log('error', error);
	}
}

async function obtenerLimitesRegiones() {
	try {
		let response = await fetch('/limites', {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json',
			},
		});
		let data = await response.json();
		let nu = groupBy(data.limites, (limite) => limite.CveRegion);
		for (let a = 1; a <= nu.size; a++) {
			var latlong = [];
			let color;
			let descripcion;
			let reg;
			for (let b = 0; b < nu.get(a).length; b++) {
				latlong.push([nu.get(a)[b].DelimLat, nu.get(a)[b].DelimLong]);
				color = nu.get(a)[b].RegionColor;
				descripcion = nu.get(a)[b].RegionDescripcion;
				reg = nu.get(a)[b].Id_kapok;
			}
			let polygon = L.polygon(latlong, { color: color, opacity: 0.75 })
				.bindPopup(descripcion)
				.addTo(mymap);
			polygonos.push({
				polygono: polygon,
				region: nu.get(a)[0].CveRegion,
				descRegion: descripcion,
				regionKapok: reg,
			});
			deferred.reject('ok');
		}
	} catch (error) {
		console.log('Error Limites', error);
		deferred.reject(new Error(data.error));
	}
}
function groupBy(list, keyGetter) {
	const map = new Map();
	list.forEach((item) => {
		const key = keyGetter(item);
		const collection = map.get(key);
		if (!collection) {
			map.set(key, [item]);
		} else {
			collection.push(item);
		}
	});
	return map;
}
// Se obtienen las camaras
async function obtenerCamaras() {
	try {
		const response = await fetch('/obtenerCapasEstaticas', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
		});
		const data = await response.json();
		if (data.error) {
			console.log('Error Camaras', data.error);
		} else {
			camaraMarkers = data;
		}
	} catch (error) {
		console.log('Error Camaras', error);
	}
}
// Se obtinenen  las unidades
async function obtenerPatrulla() {
	patrullaMarkers = new Array();
	try {
		const response = await fetch('/obtenerInfoVehiculo', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({
				reg: region,
				estatus: window.location.href,
			}),
		});
		const data = await response.json();
		if (data.error) {
		} else {
			if (region != null && region != 'null') {
				for (let i = 0; data.vehiculos.length > i; i++) {
					let vehiReg = data.vehiculos[i].region;
					if (vehiReg != null) {
						if (vehiReg.trim() == region) {
							patrullaMarkers.push(data.vehiculos[i]);
						}
					}
				}
			} else {
				patrullaMarkers = data.vehiculos;
			}
		}
	} catch (err) {
		console.log('error', err);
	}
}

async function listaUnidades() {
	try {
		const response = await fetch('/unidades', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({ reg: region }),
		});
		let data2 = new Array();
		const data = await response.json();
		data.forEach((d) => {
			data2.push({
				CveGpoOper: d.CveGpoOper.trim(),
				CveVehi: d.CveVehi,
			});
		});
		let group = groupBy(data2, (limite) => limite.CveGpoOper);
		if (data.error) {
			console.log('Error', data.error);
		} else {
			let L = document.getElementById('listaSider');
			var listaSideBar = '';
			let clase = '';
			let clase2 = '';
			group.forEach(function (element, index, array) {
				listaSideBar +=
					'<div><button class="listRegion btn btb-block dropdown-toggle" onclick="dropdown(this)" id="' +
					index.trim() +
					'">' +
					index +
					'</button></div>';
				for (let i = 0; i < element.length; i++) {
					var a = patrullaMarkers.filter(
						(e) => e.CveVehi == element[i].CveVehi
					);
					if (a[0] != null) {
						clase = 'lista';
						clase2 = 'action btn unidad';
					} else {
						clase = 'lista falla';
						clase2 = 'action btn';
					}
					listaSideBar +=
						"<li class='" +
						clase +
						' ' +
						index.trim() +
						"'>" +
						"<button class='" +
						clase2 +
						"' onclick='Zoom(this)' ondblclick='Redirect(this)' id='" +
						element[i].CveVehi +
						"' name='/asignacion/" +
						element[i].CveVehi +
						'/' +
						element[i].CveGpoOper +
						"'> " +
						"<i id='unidad' class='fas fa-car'></i>" +
						element[i].CveVehi +
						'</button>' +
						'</li>';
				}
			});
			// console.log(listaSideBar);
			L.innerHTML = listaSideBar;
		}
	} catch (error) {
		console.log('error', error);
	}
}
async function listaTabletas() {
	let list = document.getElementById('listaTablets');
	var listaTablets = '';
	let clase = '';
	let clase2 = '';
	tabletaMarkers.forEach((element) => {
		if (element.status == 'online') {
			clase = 'listaTablet';
			clase2 = 'action btn unidad';
		} else {
			clase = 'listaTablet falla';
			clase2 = 'action btn';
		}
		listaTablets +=
			"<li class='" +
			clase +
			"'>" +
			"<button class='" +
			clase2 +
			"' onclick='ZoomTablet(this)' id='" +
			element.name +
			"'> " +
			"<i id='unidad' class='fas fa-tablet-alt'></i>" +
			element.name +
			'</button>' +
			'</li>';
	});
	list.innerHTML = listaTablets;
}
// Se obtiene la ubicacion de las tabletas
async function obtenerTraccar() {
	tabletaMarkers = new Array();
	try {
		const response = await fetch('/obtenerInfoTraccar', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({
				reg: region,
			}),
		});
		const data = await response.json();
		if (data.error) {
			console.log('error', data.error);
		} else {
			tabletaMarkers = data.tabletas;
		}
	} catch (err) {
		console.log('error', err);
	}
}
//Se obtienen la lista de claves de empleados
async function obtenerInfoUnidad(boton) {
	let data = null;
	let listaPersonal = '';
	let listaEquipo = '';
	//se obtienen las personas que estan en la unidad
	let clavesPersonal = await fetch('/InfoUnidad', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
		},
		body: JSON.stringify({ CveVehi: boton.id }),
	});
	data = await clavesPersonal.json();
	$('#tituloInfo').text(
		'Unidad:' +
			data.unidad[0].CveVehi +
			'| Placa:' +
			data.unidad[0].PlacaVehi +
			'| Economico:' +
			data.unidad[0].NumEcoVehi
	);
	for (var x = 0; x < data.equipo.length; x++) {
		listaEquipo +=
			"<li class='list-group-item'>" +
			data.equipo[x].EquipoDesc +
			' ' +
			'Cantidad: ' +
			data.equipo[x].EquipoCantidad +
			'</li>';
	}
	for (var i = 0; i < data.personal.length; i++) {
		listaPersonal +=
			"<li class='list-group-item'> Empleado: " +
			data.personal[i].CveNumEmp +
			' ' +
			data.personal[i].nombre +
			'</li>';
	}
	if (data.personal[0] != null) {
		$('#ListaUnidad').html(
			"<li class='list-group-item'>" + boton.id + '</li>'
		);
		$('#ListaPersonal').html(listaPersonal);
	} else {
		$('#ListaPersonal').html('<h5>No hay informacion que mostrar</h5>');
	}
	if (data.equipo[0] != null) {
		$('#ListaEquipo').html(listaEquipo);
	} else {
		$('#ListaEquipo').html('<h5><h5/>');
	}
}

function agragarMarcadores() {
	let permisos = document.getElementById('videoUnidad');
	alertas = [];
	if (layerCamara) {
		layerCamara.clearLayers();
	}
	if (layers) {
		layers.clearLayers();
	}
	patrullas = new Array();
	layerCamara = new L.markerClusterGroup();
	layers = new L.layerGroup();
	//camaras
	if (marcadores.camaras != null) {
		for (var camara in camaraMarkers.camaras) {
			let { CamaraDescripcion, CamaraId, CamaraLat, CamaraLong } =
				camaraMarkers.camaras[camara];
			if (CamaraLat != null && CamaraLong != null) {
				layerCamara.addLayer(
					L.marker([CamaraLat, CamaraLong], {
						icon: iconCamera,
					}).bindPopup(
						"<a href='/video/" +
							CamaraId +
							"' target='_blank' class='camara btn'>" +
							CamaraDescripcion +
							'</a>'
					)
				);
			}
		}
	}
	//unidades
	if (marcadores.unidades != null) {
		for (var vehiculo in patrullaMarkers) {
			let {
				CveVehi,
				altitude,
				gpslat,
				gpslng,
				gpstime,
				recordspeed,
				region,
				speed,
				state,
				terid,
			} = patrullaMarkers[vehiculo];
			if (region != null) {
				if (gpslat != null && gpslat != null) {
					let ic = L.divIcon({
						className: 'custom-div-icon',
						html:
							"<div class='marker-pin'>" +
							CveVehi +
							"</div><img src='/images/PoliceCorrect.ico' class='img-fluid'/>",
						iconSize: [50, 62],
						iconAnchor: [30, 62],
					});
					let bindPopup = null;
					if (permisos != null) {
						bindPopup =
							"<button class='btn' data-toggle='modal' data-target='#modalInfo' onclick='obtenerInfoUnidad(this)' id='" +
							CveVehi +
							"'>" +
							CveVehi +
							'</button>' +
							"<a href='/videos/" +
							terid +
							'/' +
							CveVehi +
							"'  target='_blank' class='camara btn'> Videos" +
							'</a>' +
							'<br> ' +
							'Fercha y hora: ' +
							gpstime;
					} else {
						bindPopup =
							"<button class='btn' data-toggle='modal' data-target='#modalInfo' onclick='obtenerInfoUnidad(this)' id='" +
							CveVehi +
							"'>" +
							CveVehi +
							'</button>' +
							'</a>' +
							'<br> ' +
							'Fercha y hora: ' +
							gpstime;
					}
					if (calculardiferencia(gpstime)) {
						var mark = L.marker([gpslat, gpslng], {
							icon: iconCarFail,
						}).bindPopup(bindPopup);
					} else {
						var mark = L.marker([gpslat, gpslng], {
							icon: ic,
						}).bindPopup(bindPopup);
						if (
							region.trim() == 'R1' ||
							region.trim() == 'R2' ||
							region.trim() == 'R3' ||
							region.trim() == 'R4' ||
							region.trim() == 'R5' ||
							region.trim() == 'R6' ||
							region.trim() == 'R7' ||
							region.trim() == 'R8'
						) {
							GeoCerca(mark, region, CveVehi, gpstime);
						}

						// swal.queue(toast)
					}
					layers.addLayer(mark);
					patrullas.push({
						unidad: CveVehi,
						marcador: mark,
					});
				}
			}
		}
	}
	//tabletas
	if (marcadores.tabletas != null) {
		let mark;
		tabM = new Array();
		for (var tablet in tabletaMarkers) {
			let { latitud, longitud, name, lastupdate, CveGpoOPer } =
				tabletaMarkers[tablet];
			if (latitud != null && longitud != null) {
				mark = L.marker([latitud, longitud], {
					icon: iconTablet,
				}).bindPopup('Nombre: ' + name + '; Fecha: ' + lastupdate);
				layers.addLayer(mark);
				tabM.push({
					mark: mark,
					name: name,
				});
			}
		}
	}
	//emergencias
	if (marcadores.emergencias != null) {
		console.log(emergencias);
		for (var emergencia in emergencias) {
			let {
				CveReporte,
				DesMotiRep,
				FeHoRep,
				Prioridad,
				TRepCecomLatitud,
				TRepCecomLongitud,
				Unidades,
			} = emergencias[emergencia];
			if (TRepCecomLatitud != null && TRepCecomLongitud != null) {
				let con =
					"<div class='emergencias'> Folio: " +
					CveReporte +
					"</div><div class='emergencias'>Motivo: " +
					DesMotiRep +
					'</div>' +
					FeHoRep +
					"<div class='Eunidades'> Unidades " +
					Unidades +
					'</div>';
				switch (Prioridad) {
					case 'Alta':
						layers.addLayer(
							L.marker([TRepCecomLatitud, TRepCecomLongitud], {
								icon: iconEalta,
							}).bindPopup(con)
						);
						break;
					case 'Media':
						layers.addLayer(
							L.marker([TRepCecomLatitud, TRepCecomLongitud], {
								icon: iconEmedia,
							}).bindPopup(con)
						);
						break;
					case 'Baja':
						layers.addLayer(
							L.marker([TRepCecomLatitud, TRepCecomLongitud], {
								icon: iconEbaja,
							}).bindPopup(con)
						);
						break;
				}
			}
		}
	}
	layers.addTo(mymap);
	mymap.addLayer(layerCamara);
	countAlerts();
	insertAlerts(alertas);
}
function Redirect(url) {
	let permisos = document.getElementById('agregarPersonal');
	if (permisos != null) {
		var u = url.name;
		window.location.href = u;
	}
}

function Zoom(item) {
	var id = '';
	var mark = null;
	id = item.id;
	var iconCarO = L.icon({
		iconUrl: '/images/icono2.png',
		iconSize: [40, 40],
		iconAnchor: [15, 25],
	});
	mark = patrullas.filter((ele) => ele.unidad == id);
	if (mark[0] != null) {
		if (
			calculardiferencia(mark[0].marcador._popup._content.substr(-20, 20))
		) {
			mark[0].marcador.setIcon(iconCarFail).openPopup();
		} else {
			mark[0].marcador.setIcon(iconCarO).openPopup();
		}
		mymap.setView(mark[0].marcador.getLatLng(), 20);
	} else {
		$('#collapseAlert').collapse('show');
		item.addClasss;
		setTimeout(function () {
			$('#collapseAlert').collapse('hide');
		}, 5000);
	}
}

function ZoomTablet(item) {
	var id = '';
	var marka = null;
	id = item.id;
	var findTablet = L.icon({
		iconUrl: '/images/findTablet.png',
		iconSize: [40, 40],
		iconAnchor: [15, 25],
	});
	marka = tabM.filter((ele) => ele.name == id);
	if (marka[0] != null) {
		marka[0].mark.setIcon(findTablet).openPopup();
		mymap.setView(marka[0].mark.getLatLng(), 20);
	} else {
		$('#collapseAlert').collapse('show');
		item.addClasss;
		setTimeout(function () {
			$('#collapseAlert').collapse('hide');
		}, 5000);
	}
}

function calculardiferencia(gpstime) {
	let hoy = new Date();
	let gpst = new Date(gpstime);
	let transcurso = hoy - gpst;
	if (transcurso > 300000) {
		return true;
	} else {
		return false;
	}
}

function GeoCerca(marker, region, CveVehi, fech) {
	let polygon = polygonos.filter((ele) => ele.regionKapok == region.trim());
	let fecha;
	let CveVe;
	let regO;
	let regA;
	if (polygon.length > 0) {
		if (!polygon[0].polygono.getBounds().contains(marker.getLatLng())) {
			fecha = fech;
			CveVe = CveVehi;
			regO = region;
			for (let i = 0; i < polygonos.length; i++) {
				if (
					polygonos[i].polygono
						.getBounds()
						.contains(marker.getLatLng())
				) {
					regA = polygonos[i].descRegion;
					alertas.push({
						CveVehi: CveVe,
						fecha: fecha,
						regO: regO,
						regA: regA,
					});
					return;
				}
			}
		}
	}
}

async function insertAlerts(alerts) {
	// await fetch("/insertAlert", {
	//   method: "POST",
	//   headers: {
	//     "Content-Type": "application/json",
	//   },
	//   body: JSON.stringify({
	//     alertas: alerts
	//   }),
	// });
	localStorage.setItem('alertas', JSON.stringify(alertas));
}

function dropdown(elemento) {
	let id = elemento.id;
	if ($('.' + id).hasClass('quitar')) {
		$('.' + id).removeClass('quitar');
		$('#' + id).removeClass('dropdownCerrar');
	} else {
		$('.' + id).addClass('quitar');
		$('#' + id).addClass('dropdownCerrar');
	}
}

function marker() {
	if (document.getElementById('latP') && document.getElementById('longP')) {
		let lat = document.getElementById('latP').value;
		let long = document.getElementById('longP').value;
		mymap.setView([lat, long], 20);
	}
}
