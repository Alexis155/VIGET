var layerCamara = null;
var layers = null;
var alertas;
var tabletaMarkers = new Array();
var patrullaMarkers = new Array();
var tabletas = new Array();
var patrullas = new Array();
var polygonos = new Array();
var camaraMarkers = new Array();

var marcadores = {
  unidades: "unidades",
  tabletas: null,
  camaras: null,
};

// Iconos personalisados
var iconCamera = L.icon({
  iconUrl: "/images/unnamed.png",
  iconSize: [23, 23],
  iconAnchor: [10, 20],
});
var iconCar = L.icon({
  iconUrl: "/images/police-car-32.png",
});
var iconCarFail = L.icon({
  iconUrl: "/images/PoliceFail.ico",
  // iconSize: [25,25],
  // iconAnchor: [10,20] 
})
const iconTablet = L.icon({
  iconUrl: "/images/tablet-24.png",
  iconSize: [15, 15],
  iconAnchor: [10, 20],
});

//Pintamos el tipo de mapa sobre leaflet
// var mapaBlancoNegro = "	https://cartodb-basemaps-{s}.global.ssl.fastly.net/light_all/{z}/{x}/{y}.png ";
const basic = L.tileLayer("https://api.mapbox.com/styles/v1/kevin155/ckl6rbs691ahs17nm4kei92yb/tiles/256/{z}/{x}/{y}@2x?access_token=pk.eyJ1Ijoia2V2aW4xNTUiLCJhIjoiY2tsNnEyb29hMWtxbjJubW02NzRiN2c0biJ9.5v9L-brCDD9YxULSBEouSA", {minZoom: 3,tileSize: 512,zoomOffset: -1,attribution: "SSPMQ DIR. INF. DEP. SISTEMAS",}),
 mapBox = L.tileLayer("https://api.mapbox.com/styles/v1/kevin155/ckl6qaygo19i917nm3vp6b66k/tiles/256/{z}/{x}/{y}@2x?access_token=pk.eyJ1Ijoia2V2aW4xNTUiLCJhIjoiY2tsNnEyb29hMWtxbjJubW02NzRiN2c0biJ9.5v9L-brCDD9YxULSBEouSA", {minZoom: 3,tileSize: 512,zoomOffset: -1,attribution: "SSPMQ DIR. INF. DEP. SISTEMAS",}),
 mapSatelital = L.tileLayer("https://api.mapbox.com/styles/v1/kevin155/ckl6qg3rw0kof17ovw5r9s5v9/tiles/256/{z}/{x}/{y}@2x?access_token=pk.eyJ1Ijoia2V2aW4xNTUiLCJhIjoiY2tsNnEyb29hMWtxbjJubW02NzRiN2c0biJ9.5v9L-brCDD9YxULSBEouSA", {minZoom: 3,tileSize: 512,zoomOffset: -1,attribution: "SSPMQ DIR. INF. DEP. SISTEMAS",});
var mymap = L.map("qroMap", {
  center: [20.58899, -100.390893],
  zoom: 15,
  scrollWheelZoom: true,
  layers: [basic]
});
var baseMaps = {
  "Basico": basic,
  "Calles": mapBox,
  "Satelidtal": mapSatelital
};
L.control.layers(baseMaps).addTo(mymap);
obtenerLimitesRegiones();
obtenerCamaras();
obtenerTraccar();
setTimeout(obtenerPatrulla, 200);
setTimeout(listaUnidades, 500);

//Se obtienen los limitis de las regiones
async function obtenerLimitesRegiones() {
  try {
    let response = await fetch("/limites", {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      }
    });
    let data = await response.json();
    let nu = groupBy(data.limites, limite => limite.CveRegion);
    for (let a = 1; a <= nu.size; a++){
      var latlong = [];
      let color;
      let descripcion;
      let reg;
      for (let b = 0; b < nu.get(a).length; b++){
        latlong.push([nu.get(a)[b].DelimLat, nu.get(a)[b].DelimLong]);
        color = nu.get(a)[b].RegionColor;
        descripcion = nu.get(a)[b].RegionDescripcion;
        reg = nu.get(a)[b].Id_kapok;
      }
      let polygon = L.polygon(latlong, { color: color,  opacity: 0.75 }).bindPopup(descripcion).addTo(mymap);
      polygonos.push({
        polygono: polygon,
        region: nu.get(a)[0].CveRegion,
        descRegion: descripcion,
        regionKapok: reg
      })
    }
  } catch (error) {
    console.log("Error Limites", error)
  }
}

function groupBy(list, keyGetter) {
  const map = new Map();
  list.forEach((item) => {
       const key = keyGetter(item);
       const collection = map.get(key);
       if (!collection) {
           map.set(key, [item]);
       } else {
           collection.push(item);
       }
  });
  return map;
}

function countAlerts() {
  try {
    if (alertas.length > 0) {
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: true,
        timer: 15000,
        timerProgressBar: true,
        showCancelButton: true,
      })
      Toast.fire({
        icon: 'warning',
        title: 'Hay ' + alertas.length + ' de alertas',
        confirmButtonText: 'Ver',
        cancelButtonText: 'Cancelar'
      }).then((result) => {
        if (result.isConfirmed) {
          // window.location.href = "alertas";
          window.open('alertas');
        }
      })
    }
   
  } catch (error) {
    console.log("Error", error)
  }
}

// Se obtienen las camaras
async function obtenerCamaras() {
  try {
    const response = await fetch("/obtenerCapasEstaticas", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
    });
    const data = await response.json();
    if (data.error) {
      console.log("Error Camaras", data.error)
    } else {
      camaraMarkers = data;  
    }
  } catch (error) {
    console.log("Error Camaras", error);
  }
}

// Se obtiene la ubicacion de las tabletas
async function obtenerTraccar() {
  try {
    const response = await fetch("/obtenerInfoTraccar", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        reg: region,
      }),
    });
    const data = await response.json();
    if (data.error) {
      console.log("error", data.error)
    } else {
      tabletaMarkers = data.tabletas;
      setTimeout(obtenerTraccar, 30000);  
    }
  } catch (err) {
    console.log("error", err)
  }
}

// Se obtinenen  las unidades
async function obtenerPatrulla() {
  try {
    const response = await fetch("/obtenerInfoVehiculo", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        reg: region,
        estatus: window.location.href
      }),
    });
    const data = await response.json();
    if (data.error) {
      console.log("error", data.error)
    } else {
      // console.log(data.vehiculos);
      if (region[1]) {
        patrullaMarkers = data.vehiculos.filter((vehiculo) => vehiculo.regionId == region[1]);
      } else {
        patrullaMarkers = data.vehiculos;
      }
      agragarMarcadores();
      setTimeout(obtenerPatrulla, 30000);  
    }
  } catch (err) {
    console.log("error", err)
  } 
}

//Aqui se agrega los marcadores de las al layer
function agragarMarcadores() {
  alertas = [];
  if (layerCamara) {
    layerCamara.clearLayers();
  }
  if (layers) {
    layers.clearLayers();
  }
  patrullas = new Array();
  layerCamara = new L.markerClusterGroup();
  layers = new L.layerGroup();
  //camaras
  if (marcadores.camaras != null) {
    for (var camara in camaraMarkers.camaras) {
      let {
        CamaraDescripcion,
        CamaraId,
        CamaraLat,
        CamaraLong,
      } = camaraMarkers.camaras[camara];
      if (CamaraLat != null && CamaraLong != null) {
        layerCamara.addLayer(
          L.marker([CamaraLat, CamaraLong], { icon: iconCamera })
            .bindPopup(
              "<a href='/video/"+CamaraId+"' target='_blank' class='camara btn'>" +
              CamaraDescripcion +
              "</a>"
            )
        );
      }
    }
  }
  //unidades
  if (marcadores.unidades != null) {
    for (var vehiculo in patrullaMarkers) {
      let { gpslat, gpslng, gpstime, terid, time, carlicence, regionId } = patrullaMarkers[
        vehiculo
      ];
      if (gpslat != null && gpslat != null) {
        let ic = L.divIcon({className: 'custom-div-icon',html: "<div class='marker-pin'>"+carlicence+"</div><img src='images/PoliceCorrect.ico' class='img-fluid'/>", iconSize: [50, 62],iconAnchor: [30, 62]})
        if (calculardiferencia(gpstime)) {
          var mark = L.marker([gpslat, gpslng], { icon: iconCarFail }).bindPopup(
            "<button class='btn' data-toggle='modal' data-target='#modalInfo' onclick='obtenerInfoUnidad(this)' id='" +
            carlicence +
            "'>" +
            carlicence +
            "</button>" +
            "<a href='/videos/" + terid + "/" + carlicence + "'  target='_blank' class='camara btn'> Videos" +
            "</a>" +
            "<br> " +
            "Fercha y hora: " + gpstime
            // "Unidad: " + carlicence
          );
        } else {
          var mark = L.marker([gpslat, gpslng], { icon: ic }).bindPopup(
            "<button class='btn' data-toggle='modal' data-target='#modalInfo' onclick='obtenerInfoUnidad(this)' id='" +
            carlicence +
            "'>" +
            carlicence +
            "</button>" +
            "<a href='/videos/" + terid + "/" + carlicence + "'  target='_blank' class='camara btn'> Videos" +
            "</a>" +
            "<br> " +
            "Fercha y hora: " + gpstime
            // "Unidad: " + carlicence
          );
          if (regionId == 4 || regionId == 5 || regionId == 8 || regionId == 9 || regionId == 10 || regionId == 11 || regionId == 12 || regionId == 13) {
            GeoCerca(mark, regionId, carlicence, gpstime);  
          }
          
          // swal.queue(toast)
        }
        layers.addLayer(mark);
        patrullas.push({
          unidad: carlicence,
          marcador: mark,
        });
      }
    }
  }
  //tabletas
  if (marcadores.tabletas != null) {
    for (var tablet in tabletaMarkers) {
      let { latitud, longitud, name, lastupdate, CveGpoOPer } = tabletaMarkers[
        tablet
      ];
      if (latitud != null && longitud != null) {
        layers.addLayer(
          L.marker([latitud, longitud], { icon: iconTablet }).bindPopup(
            "Nombre: " + name + "; Fecha: " + lastupdate
          )
        );
      }
    }
  }
  layers.addTo(mymap);
  mymap.addLayer(layerCamara);
  insertAlerts(alertas);
}

//Se obtienen la lista de claves de empleados
async function obtenerInfoUnidad(boton) {
  console.log(boton)
  let data = null;
  let listaPersonal = "";
  let listaEquipo = "";
  //se obtienen las personas que estan en la unidad
  let clavesPersonal = await fetch("/InfoUnidad", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ CveVehi: boton.id }),
  });
  data = await clavesPersonal.json();
  $("#tituloInfo").text("Unidad:" + data.unidad[0].CveVehi+"| Placa:"+data.unidad[0].PlacaVehi+"| Economico:"+data.unidad[0].NumEcoVehi);
  for (var x = 0; x < data.equipo.length; x++) { 
    listaEquipo += "<li class='list-group-item'>" + data.equipo[x].EquipoDesc +" " +"Cantidad: " + data.equipo[x].EquipoCantidad +"</li>";
  }
  for (var i = 0; i < data.personal.length; i++) {
    listaPersonal += "<li class='list-group-item'> Empleado: "+ data.personal[i].CveNumEmp+" "+ data.personal[i].nombre + "</li>";
  }
  if ( data.personal[0] != null) {
    $("#ListaUnidad").html("<li class='list-group-item'>" + boton.id + "</li>");
    $("#ListaPersonal").html(listaPersonal);
  } else {
    $("#ListaPersonal").html("<h5>No hay informacion que mostrar</h5>");
  }
  if (data.equipo[0] != null) {
    $("#ListaEquipo").html(listaEquipo);
  } else {
    $("#ListaEquipo").html("<h5><h5/>")
  }
}


async function listaUnidades() {
  try {
    const response = await fetch("/unidades", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ reg: region[0] }),
    });
    const data = await response.json();
    if (data.error) {
      console.log("Error",data.error)
    } else {
      let L = document.getElementById("listaSider");
    var listaSideBar = "";
    let clase = ""
    let clase2 = "";
    for (var i = 0; i < data.length; i++) {
      var a = patrullaMarkers.filter((e) => e.carlicence == data[i].CveVehi);
      if (a[0] != null) {
        clase = "lista"
        clase2 = "action btn unidad";
      } else {
        clase = "lista falla";
        clase2 = "action btn";
      }
      listaSideBar += "<li class='"+clase+"'>" +
        "<button class='"+clase2+"' onclick='Zoom(this)' ondblclick='Redirect(this)' id='"+data[i].CveVehi+"' name='/asignacion/"+ data[i].CveVehi+"/"+data[i].CveGpoOper+"'> " +
        "<i id='unidad' class='fas fa-car'></i>" +
        data[i].CveVehi +
        "</button>" +
        "</li>";
    }
    L.innerHTML = listaSideBar;
    }
  } catch (error) {
    console.log("error", error);
  }
}

function Region() {
  let separador = $("#regiones").val();
  region = separador.split(":");
  obtenerPatrulla();
  setTimeout(listaUnidades,500)
}

document.getElementById("unidades").onchange = function () {
  if ($(this).prop("checked") != true) {
    marcadores.unidades = null;
    agragarMarcadores();
  } else {
    marcadores.unidades = "unidades";
    agragarMarcadores();
  }
};

document.getElementById("tabletas").onchange = function () {
  if ($(this).prop("checked") != true) {
    marcadores.tabletas = null;
    agragarMarcadores();
  } else {
    marcadores.tabletas = "tabletas";
    agragarMarcadores();
  }
};

document.getElementById("cams").onchange = function () {
  if ($(this).prop("checked") != true) {
    marcadores.camaras = null;
    agragarMarcadores();
  } else {
    marcadores.camaras = "camaras";
    agragarMarcadores();
  }
};

// Buscador
$("#buscar").on("keyup", function () {
  var patron = $(this).val();

  // si el campo está vacío
  if (patron == "") {
    // mostrar todos los elementos
    $(".lista").css("display", "list-item");

    // si tiene valores, realizar la búsqueda
  } else {
    // atravesar la lista
    $(".lista").each(function () {
      if ($(this).text().indexOf(patron) < 0) {
        // si el texto NO contiene el patrón de búsqueda, esconde el elemento
        $(this).css("display", "none");
      } else {
        // si el texto SÍ contiene el patrón de búsqueda, muestra el elemento
        $(this).css("display", "list-item");
      }
    });
  }
});

//funcion para redirigir
function Redirect(url) {
  var u = url.name;
  window.location.href = u;
}

//Funcion para acer zoom a la unidad elegida
function Zoom(item) {
  var id = "";
  var mark = null;
  id = item.id;
  console.log("id", id)
  var iconCarO = L.icon({
    iconUrl: "/images/icono2.png",
    iconSize: [40, 40],
    iconAnchor: [15, 25],
  });
  mark = patrullas.filter((ele) => ele.unidad == id);
  if (mark[0] != null) {
    if (calculardiferencia(mark[0].marcador._popup._content.substr(-20, 20))) {
      mark[0].marcador.setIcon(iconCarFail).openPopup();
    } else {
      mark[0].marcador.setIcon(iconCarO).openPopup(); 
    }
    mymap.setView(mark[0].marcador.getLatLng(), 20);
  } else {
    $("#collapseAlert").collapse("show");
    item.addClasss
    setTimeout(function(){ $("#collapseAlert").collapse('hide'); }, 5000);
  }
}

function calculardiferencia(gpstime) {
  let hoy = new Date();
  let gpst = new Date(gpstime);
  let transcurso = hoy - gpst;
  if (transcurso>300000) {
    return true
  } else {
    return false
  }
}

function GeoCerca(marker, region, carlicence, fech) {
  let polygon = polygonos.filter((ele) => ele.regionKapok == region);
  let fecha;
  let CveVehi;
  let regO;
  let regA;
  if (polygon.length > 0) {
    if (!polygon[0].polygono.getBounds().contains(marker.getLatLng())) {
      fecha = fech;
      CveVehi = carlicence;
      regO = polygon[0].descRegion;
      for (let i = 0; i < polygonos.length; i++){
        if (polygonos[i].polygono.getBounds().contains(marker.getLatLng())) {
           regA = polygonos[i].descRegion
          alertas.push({
            CveVehi: CveVehi,
            fecha: fecha,
            regO: regO,
            regA: regA
          })
          break;
        }
      }
    }
  }
}

async function insertAlerts(alerts) {
  await fetch("/insertAlert", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      alertas: alerts
    }),
  }).then((response) => {
    if (response.status == 200) {
      for (let i = 0; alertas.length > i; i++){
        let marcador = patrullas.filter((ele) => ele.unidad == alertas[i].CveVehi);
        let ic = L.divIcon({className: 'custom-div-icon',html: "<div class='marker-pin'>"+alertas[i].CveVehi+"</div><img src='images/AlertIcon.ico' class='img-fluid'/>", iconSize: [50, 62],iconAnchor: [30, 62]})
        marcador[0].marcador.setIcon(ic);
      }
      countAlerts();
    }
  });
}