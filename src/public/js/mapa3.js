const basic = L.tileLayer(
		'https://api.mapbox.com/styles/v1/kevin155/ckl6rbs691ahs17nm4kei92yb/tiles/256/{z}/{x}/{y}@2x?access_token=pk.eyJ1Ijoia2V2aW4xNTUiLCJhIjoiY2tsNnEyb29hMWtxbjJubW02NzRiN2c0biJ9.5v9L-brCDD9YxULSBEouSA',
		{
			minZoom: 3,
			tileSize: 512,
			zoomOffset: -1,
			attribution: 'SSPMQ DIR. INF. DEP. SISTEMAS',
		}
	),
	mapBox = L.tileLayer(
		'https://api.mapbox.com/styles/v1/kevin155/ckl6qaygo19i917nm3vp6b66k/tiles/256/{z}/{x}/{y}@2x?access_token=pk.eyJ1Ijoia2V2aW4xNTUiLCJhIjoiY2tsNnEyb29hMWtxbjJubW02NzRiN2c0biJ9.5v9L-brCDD9YxULSBEouSA',
		{
			minZoom: 3,
			tileSize: 512,
			zoomOffset: -1,
			attribution: 'SSPMQ DIR. INF. DEP. SISTEMAS',
		}
	),
	mapSatelital = L.tileLayer(
		'https://api.mapbox.com/styles/v1/kevin155/ckl6qg3rw0kof17ovw5r9s5v9/tiles/256/{z}/{x}/{y}@2x?access_token=pk.eyJ1Ijoia2V2aW4xNTUiLCJhIjoiY2tsNnEyb29hMWtxbjJubW02NzRiN2c0biJ9.5v9L-brCDD9YxULSBEouSA',
		{
			minZoom: 3,
			tileSize: 512,
			zoomOffset: -1,
			attribution: 'SSPMQ DIR. INF. DEP. SISTEMAS',
		}
	);

var mymap = L.map('qroMap', {
	center: [20.58899, -100.390893],
	zoom: 13,
	scrollWheelZoom: true,
	layers: [basic],
});
var baseMaps = {
	Basico: basic,
	Calles: mapBox,
	Satelital: mapSatelital,
};
var overlayMaps = null;
const iconCar = L.icon({
	iconUrl: '/images/police-car-32.png',
});
const iconCarFail = L.icon({
	iconUrl: '/images/PoliceFail.ico',
});
const iconEalta = L.icon({
	iconUrl: '/images/alta.gif',
	iconSize: [30, 30],
	iconAnchor: [8, 16],
});
const iconEmedia = L.icon({
	iconUrl: '/images/media.gif',
	iconSize: [45, 45],
	iconAnchor: [8, 16],
});
const iconEbaja = L.icon({
	iconUrl: '/images/baja.gif',
	iconSize: [45, 45],
	iconAnchor: [8, 16],
});
const iconTablet = L.icon({
	iconUrl: '/images/tablet-24.png',
	iconSize: [15, 15],
	iconAnchor: [8, 16],
});
const iconMoto = L.icon({
	iconUrl: '/images/moto2-32.png',
	iconSize: [20, 20],
	iconAnchor: [8, 8],
});
const iconGps = L.icon({
	iconUrl: '/images/coche.png',
	iconSize: [23, 23],
	iconAnchor: [8, 16],
});
const iconRadio = L.icon({
	iconUrl: '/images/radio32.png',
});
// L.control.layers(baseMaps, overlayMaps).addTo(mymap);
var dispositivosListado = null;
var region = null;
var layerP = null;
layerP = new L.layerGroup();
var LayerPo = null;
var layerT = null;
layerT = new L.layerGroup();
var layerM = null;
layerM = new L.layerGroup();
var layerG = null;
var layerE = null;
layerE = new L.layerGroup();
var layerCamara = null;
var layerBotones = null;
var layerCentro = null;
var layerCH = null;
var layerEG = null;
var layerFCP = null;
var layerFO = null;
var layerJVH = null;
var layerSRJ = null;
var layerVCR = null;
var layerVi = null;
var tabs = null;
var mots = null;
var cams = null;
var emer = null;
var gpsE = null;
let pat = null;
var hoy = new Date();
// var emergencias;
var marcadores = {
	unidades: 'unidades',
	tabletas: 'tabletas',
	motocicletas: 'motocicletas',
	camaras: null,
	emergencias: 'emergencias',
	gps: null,
};
var alertas;

$(document).ready(function () {
	marker();
	//Carga tabletas

	$.ajax({
		url: '/obtenerInfoTraccar',
		type: 'POST',
		dataType: 'json',
		success: function (tablets) {
			let grupoTablets = [];
			let grupoMotos = [];
			for (let i = 0; i < tablets.tabletas.length; i++) {
				if (tablets.tabletas[i].name.includes('SP-')) {
					grupoMotos.push(tablets.tabletas[i]);
				} else {
					grupoTablets.push(tablets.tabletas[i]);
				}
			}
			console.log('Motos:', grupoMotos);
			console.log('Tabletas:', grupoTablets);
			mots = grupoMotos;
			tabs = grupoTablets;
			Tabletas(grupoTablets);
			Motocicletas(grupoMotos);
		},
		fail: function () {
			console.log('Error al obtener tabletas');
		},
	});

	$.ajax({
		url: '/limites',
		type: 'GET',
		dataType: 'json',
		success: function (limites) {
			Limites(limites);
		},
		fail: function () {
			console.log('Error al obtener');
		},
	}).done(function () {
		$.ajax({
			url: '/obtenerInfoVehiculo',
			type: 'POST',
			dataType: 'json',
			data: {
				reg: region,
			},
			success: function (vehiculos) {
				pat = vehiculos;
				Patrullas(vehiculos, false);
			},
			fail: function () {
				console.log('Error al obtener vehiculos');
			},
		}).done(function () {
			$.ajax({
				url: '/emergencias',
				type: 'GET',
				dataType: 'json',
				success: function (emergencias) {
					emer = emergencias;
					if (
						region == 'R1' ||
						region == 'R2' ||
						region == 'R3' ||
						region == 'R4' ||
						region == 'R5' ||
						region == 'R6' ||
						region == 'R7' ||
						region == 'R8'
					) {
						emergencias = emergencias.filter((e) => 'R' + e.Region == region);
					}
					Emergencias(emergencias, false);
				},
				fail: function () {
					console.log('Error al obtener emergencias');
				},
			}).done(function () {
				$.ajax({
					url: '/unidades',
					type: 'POST',
					dataType: 'json',
					data: {
						reg: region,
					},
					success: function (list) {
						listado(list, tabs, mots);
					},
					fail: function () {
						console.log('Error al obtener lista de vehiculos');
					},
				}).done(function () {
					$.ajax({
						url: '/obtenerCapasEstaticas',
						type: 'POST',
						dataType: 'json',
						success: function (camaras) {
							cams = camaras;
							Camaras(camaras);
						},
						fail: function () {
							console.log('Error al obtener camaras');
						},
					}).done(function () {
						$.ajax({
							url: '/botones',
							type: 'GET',
							dataType: 'json',
							success: function (botones) {
								Botones(botones.botones);
							},
							fail: function (error) {
								console.log('Falló en botones', error);
							},
						}).done(function () {
							$.ajax({
								url: '/centros',
								type: 'GET',
								dataType: 'json',
								success: function (centros) {
									CentroSalud(centros);
								},
								fail: function (error) {
									console.log('Error centros de salud', error);
								},
							}).done(function () {
								$.ajax({
									url: '/luminarias',
									type: 'GET',
									dataType: 'json',
									success: function (luminarias) {
										Luminarias(luminarias);
									},
									fail: function (error) {
										console.log('Error en luminarias', error);
									},
								}).done(function () {
									$.ajax({
										url: '/lectores',
										type: 'POST',
										dataType: 'json',
										success: function (res) {
											lectores(res);
										},
										fail: function (e) {
											console.log('Falló en lectores', e);
										},
									}).done(function () {
										layerMaps(), setTimeout(recarga, 60000);
									});
								});
							});
						});
					});
				});
			});
		});
	});

	function recarga() {
		console.log('Comienzo de actualización de datos..');
		console.log('1.- Cargando datos de unidades...');
		$.ajax({
			url: '/obtenerInfoVehiculo',
			type: 'POST',
			dataType: 'json',
			data: {
				reg: region,
			},
			success: function (vehiculos) {
				Patrullas(vehiculos, false);
			},
			fail: function () {
				console.log('falla recarga vehiculos');
			},
		}).done(function () {
			console.log('2.- Cargando datos de emergencias...');
			$.ajax({
				url: '/emergencias',
				type: 'GET',
				dataType: 'json',
				success: function (emergencias) {
					emer = emergencias;
					if (
						region == 'R1' ||
						region == 'R2' ||
						region == 'R3' ||
						region == 'R4' ||
						region == 'R5' ||
						region == 'R6' ||
						region == 'R7' ||
						region == 'R8'
					) {
						emergencias = emergencias.filter((e) => 'R' + e.Region == region);
					}
					Emergencias(emergencias, false);
				},
				fail: function () {
					console.log('Falla recarga de emergencias');
				},
			}).done(function () {
				console.log('3.- Cargando datos de tabletas...');
				$.ajax({
					url: '/obtenerInfoTraccar',
					type: 'POST',
					dataType: 'json',
					success: function (tablets) {
						let grupoTablets = [];
						let grupoMotos = [];
						for (let i = 0; i < tablets.tabletas.length; i++) {
							if (tablets.tabletas[i].name.includes('SP-')) {
								grupoMotos.push(tablets.tabletas[i]);
							} else {
								grupoTablets.push(tablets.tabletas[i]);
							}
						}
						console.log('Motos:', grupoMotos);
						console.log('Tabletas:', grupoTablets);
						mots = grupoMotos;
						tabs = grupoTablets;
						Tabletas(grupoTablets);
						Motocicletas(grupoMotos);
					},
					fail: function () {
						console.log('falla recarga de tabletas');
					},
				}).done(function () {
					console.log('4.- Cargando datos de la lista de unidades...');
					$.ajax({
						url: '/unidades',
						type: 'POST',
						dataType: 'json',
						data: {
							reg: region,
						},
						success: function (list) {
							listado(list, tabs, mots);
						},
						fail: function () {
							console.log('falla recarga lista de unidades');
						},
					}).done(function () {
						console.log('5.- Cargando datos de la lista de lectores...');
						$.ajax({
							url: '/lectores',
							type: 'POST',
							dataType: 'json',
							success: function (res) {
								lectores(res);
							},
							fail: function (e) {
								console.log('Falló en lectores', e);
							},
						}).done(function () {
							console.log('¡Actualización de datos exitoso!');
							setTimeout(recarga, 60000);
						});
					});
				});
			});
		});
	}

	// Buscador
	$('#buscar').on('keyup', function () {
		var patron = $(this).val();
		// si el campo está vacío
		if (patron == '') {
			// mostrar todos los elementos
			$('.lista').css('display', 'flex');
			// si tiene valores, realizar la búsqueda
		} else {
			// atravesar la lista
			$('.lista').each(function () {
				if ($(this).text().indexOf(patron) < 0) {
					// si el texto NO contiene el patrón de búsqueda, esconde el elemento
					$(this).css('display', 'none');
				} else {
					// si el texto SÍ contiene el patrón de búsqueda, muestra el elemento
					$(this).css('display', 'flex');
				}
			});
		}
	});
	// Buscador
	$('#buscarLector').on('keyup', function () {
		var patron = $(this).val();
		// si el campo está vacío
		if (patron == '') {
			// mostrar todos los elementos
			$('.lista').css('display', 'flex');
			// si tiene valores, realizar la búsqueda
		} else {
			// atravesar la lista
			$('.lista').each(function () {
				if ($(this).text().indexOf(patron) < 0) {
					// si el texto NO contiene el patrón de búsqueda, esconde el elemento
					$(this).css('display', 'none');
				} else {
					// si el texto SÍ contiene el patrón de búsqueda, muestra el elemento
					$(this).css('display', 'flex');
				}
			});
		}
	});
});

function layerMaps() {
	overlayMaps = {};

	var layerControl = L.control.layers(baseMaps, overlayMaps).addTo(mymap);

	let emer = document.getElementById('emer');
	if (emer != null) {
		layerControl.addOverlay(layerE, 'Emergencias');
	}

	let uni = document.getElementById('uni');
	if (uni != null) {
		layerControl.addOverlay(layerP, 'Unidades');
		layerControl.addOverlay(layerM, 'Motocicletas');
	}

	let table = document.getElementById('table');
	if (table != null) {
		layerControl.addOverlay(layerT, 'Tabletas');
	}

	layerControl.addOverlay(layerCentro, 'Centros de salud');
	layerControl.addOverlay(layerCamara, 'Camaras');
	layerControl.addOverlay(layerCH, 'Luminarias');
	layerControl.addOverlay(layerBotones, 'Botones');
	layerControl.addOverlay(LayerPo, 'Limites');
}

function selectRegion(valor) {
	$('#modal-filtro').modal('hide');
	region = valor;
	mymap.removeLayer(layerP);
	$.ajax({
		url: '/obtenerInfoVehiculo',
		type: 'POST',
		dataType: 'json',
		data: {
			reg: region,
		},
		success: function (vehiculos) {
			Patrullas(vehiculos, true);
		},
		fail: function (error) {
			console.log('Surgió un fallo al cambiar de región');
		},
	}).done(function () {
		$.ajax({
			url: '/unidades',
			type: 'POST',
			dataType: 'json',
			data: {
				reg: region,
			},
			success: function (list) {
				listado(list);
			},
		}).done(function () {
			if (
				region == 'R1' ||
				region == 'R2' ||
				region == 'R3' ||
				region == 'R4' ||
				region == 'R5' ||
				region == 'R6' ||
				region == 'R7' ||
				region == 'R8'
			) {
				let emergencias = emer.filter((e) => 'R' + e.Region == region);
				Emergencias(emergencias, true);
			} else {
				Emergencias(emer, true);
			}
		});
	});
}

function marker() {
	if (document.getElementById('latP') && document.getElementById('longP')) {
		let lat = document.getElementById('latP').value;
		let long = document.getElementById('longP').value;
		mymap.setView([lat, long], 20);
	}
}

// Limites
var polygonos = new Array();
LayerPo = new L.layerGroup();
function Limites(data) {
	let val = groupBy(data.limites, (limite) => limite.CveRegion);
	for (let a = 1; a <= val.size; a++) {
		var latlong = [];
		let color;
		let descripcion;
		let reg;
		for (let b = 0; b < val.get(a).length; b++) {
			latlong.push([val.get(a)[b].DelimLat, val.get(a)[b].DelimLong]);
			color = val.get(a)[b].RegionColor;
			descripcion = val.get(a)[b].RegionDescripcion;
			reg = val.get(a)[b].Id_kapok;
		}
		let polygon = L.polygon(latlong, {
			color: color,
			opacity: 0.75,
		}).bindPopup(descripcion);
		LayerPo.addLayer(polygon).addTo(mymap);
		polygonos.push({
			polygono: polygon,
			region: val.get(a)[0].CveRegion,
			descRegion: descripcion,
			regionKapok: reg,
		});
	}
}

//patrullas---
var patrullaMarkers;
function Patrullas(data, isFiltrado) {
	patrullaMarkers = new Array();
	if (region != null && region != 'null') {
		for (let i = 0; data.vehiculos.length > i; i++) {
			let vehiReg = data.vehiculos[i].region;
			if (vehiReg != null) {
				if (vehiReg.trim() == region) {
					patrullaMarkers.push(data.vehiculos[i]);
				}
			}
		}
	} else {
		patrullaMarkers = data.vehiculos;
	}

	MarcadorPatrulla(patrullaMarkers, isFiltrado);
}

var patrullas;
let mostrarUnidadesInicio = true;
function MarcadorPatrulla(dataUnidades, isFiltrado) {
	if (mostrarUnidadesInicio) {
		mymap.addLayer(layerP);
		mostrarUnidadesInicio = false;
	}
	layerP.clearLayers();
	alertas = [];
	let permisos = document.getElementById('videoUnidad');
	patrullas = new Array();
	if (marcadores.unidades != null) {
		for (var vehiculo in dataUnidades) {
			let { CveVehi, altitude, gpslat, gpslng, gpstime, recordspeed, region, speed, state, terid } = dataUnidades[vehiculo];
			if (region != null) {
				if (gpslat != null && gpslat != null) {
					let ic = L.divIcon({
						className: 'custom-div-icon',
						html: "<div class='marker-pin'>" + CveVehi + "</div><img src='/images/PoliceCorrect.ico' class='img-fluid'/>",
						iconSize: [45, 55],
						iconAnchor: [4, 5],
					});
					let bindPopup = null;
					if (permisos != null) {
						bindPopup =
							"<button class='btn' data-toggle='modal' data-target='#modalInfo' onclick='obtenerInfoUnidad(this)' id='" +
							CveVehi +
							"'>" +
							CveVehi +
							'</button>' +
							"<a href='/videos/" +
							terid +
							'/' +
							CveVehi +
							"'  target='_blank' class='camara btn'> Videos" +
							'</a>' +
							'<br>' +
							'Region: ' +
							region +
							'<br> ' +
							'Fecha y hora: ' +
							gpstime +
							'<br> ' +
							'Latitud y Longitud: ' +
							gpslat +
							', ' +
							gpslng;
					} else {
						bindPopup =
							"<button class='btn' data-toggle='modal' data-target='#modalInfo' onclick='obtenerInfoUnidad(this)' id='" +
							CveVehi +
							"'>" +
							CveVehi +
							'</button>' +
							'</a>' +
							'<br> ' +
							'Fecha y hora: ' +
							gpstime +
							'<br> ' +
							'Latitud y Longitud: ' +
							gpslat +
							', ' +
							gpslng;
					}
					if (calculardiferencia(gpstime)) {
						var mark = L.marker([gpslat, gpslng], {
							icon: iconCarFail,
						}).bindPopup(bindPopup);
					} else {
						var mark = L.marker([gpslat, gpslng], {
							icon: ic,
						}).bindPopup(bindPopup);
						if (
							region.trim() == 'R1' ||
							region.trim() == 'R2' ||
							region.trim() == 'R3' ||
							region.trim() == 'R4' ||
							region.trim() == 'R5' ||
							region.trim() == 'R6' ||
							region.trim() == 'R7' ||
							region.trim() == 'R8'
						) {
							GeoCerca(mark, region, CveVehi, gpstime);
						}

						// swal.queue(toast)
					}
					layerP.addLayer(mark);
					patrullas.push({
						unidad: CveVehi,
						marcador: mark,
					});
				}
			}
		}
	}
	if (isFiltrado) {
		mymap.addLayer(layerP);
	}
	countAlerts();
	insertAlerts(alertas);
}

function calculardiferencia(gpstime) {
	let hoy = new Date();
	let gpst = new Date(gpstime);
	let transcurso = hoy - gpst;
	if (transcurso > 420000) {
		return true;
	} else {
		return false;
	}
}

function GeoCerca(marker, region, CveVehi, fech) {
	let polygon = polygonos.filter((ele) => ele.regionKapok == region.trim());
	let fecha;
	let CveVe;
	let regO;
	let regA;
	if (polygon.length > 0) {
		if (!polygon[0].polygono.getBounds().contains(marker.getLatLng())) {
			fecha = fech;
			CveVe = CveVehi;
			regO = region;
			for (let i = 0; i < polygonos.length; i++) {
				if (polygonos[i].polygono.getBounds().contains(marker.getLatLng())) {
					regA = polygonos[i].descRegion;
					alertas.push({
						CveVehi: CveVe,
						fecha: fecha,
						regO: regO,
						regA: regA,
					});
					return;
				}
			}
		}
	}
}

//Emergencias
let mostrarEmergenciasInicio = true;
function Emergencias(emergencias, isFiltrado) {
	if (mostrarEmergenciasInicio) {
		mymap.addLayer(layerE);
		mostrarEmergenciasInicio = false;
	}
	layerE.clearLayers();
	if (marcadores.emergencias != null) {
		for (var emergencia in emergencias) {
			let { CveReporte, DesMotiRep, FeHoRep, Prioridad, TRepCecomLatitud, TRepCecomLongitud, Unidades } = emergencias[emergencia];
			if (TRepCecomLatitud != null && TRepCecomLongitud != null) {
				let con =
					"<div class='emergencias'><a href='http://172.23.176.5/cecomwebvista/inicio.aspx?OPEREZ,1," +
					CveReporte +
					"'  target='_blank'>Folio: " +
					CveReporte +
					"</a></div><div class='emergencias'>Motivo: " +
					DesMotiRep +
					'</div>' +
					FeHoRep +
					"<div class='Eunidades'> Unidades " +
					Unidades +
					"</div> <div class='Eunidades'> Latitud y Longitud " +
					TRepCecomLatitud +
					', ' +
					TRepCecomLongitud +
					'</div>';
				switch (Prioridad) {
					case 'Alta':
						layerE.addLayer(
							L.marker([TRepCecomLatitud, TRepCecomLongitud], {
								icon: iconEalta,
							}).bindPopup(con)
						);
						break;
					case 'Media':
						layerE.addLayer(
							L.marker([TRepCecomLatitud, TRepCecomLongitud], {
								icon: iconEmedia,
							}).bindPopup(con)
						);
						break;
					case 'Baja':
						layerE.addLayer(
							L.marker([TRepCecomLatitud, TRepCecomLongitud], {
								icon: iconEbaja,
							}).bindPopup(con)
						);
						break;
				}
			}
		}
	}
	if (isFiltrado) {
		mymap.addLayer(layerE);
	}
}

const listado = (unidades) => {
	console.log('Unidades;', unidades);
	dispositivosListado = '';
	let data2 = new Array();
	for (let i = 0; i < unidades.length; i++) {
		data2.push({
			CveGpoOper: unidades[i].CveGpoOper.trim(),
			CveVehi: unidades[i].CveVehi,
		});
	}
	let group = groupBy(data2, (limite) => limite.CveGpoOper);

	let L = document.getElementById('listado');
	var listaSideBar = '';
	group.forEach(function (element, index, array) {
		for (let i = 0; i < element.length; i++) {
			listaSideBar +=
				"<div class='row lista btn-list btn'>" +
				"<button class='btn-list btn btn-light btn col-11 align-items-center'" +
				"' onclick='Zoom(this)' id='" +
				element[i].CveVehi +
				"' name='/asignacion/" +
				element[i].CveVehi +
				'/' +
				element[i].CveGpoOper +
				"'> <span class='online'>" +
				element[i].CveVehi +
				' - ' +
				index +
				'</span><i class="fas fa-car"></i></button> ' +
				"<button type='button' class='btn btn-light col-1 align-items-center'" +
				"onclick='Redirect(this)' name='/asignacion/" +
				element[i].CveVehi +
				'/' +
				element[i].CveGpoOper +
				"'><i class='fas fa-user fa-sm'></i></button></div>";
		}
	});
	var listaTablets = '';
	let clase = '';
	tabs.forEach((element) => {
		listaTablets +=
			"<button class='lista btn-list btn btn-light btn btn-block " +
			' align-items-center' +
			clase +
			"' onclick='ZoomTablet(this)' id='" +
			element.name +
			"'> ";
		element.status == 'online'
			? (listaTablets += "<span class='online'>" + element.name + ' On ')
			: (listaTablets += "<span class='offline'>" + element.name + ' Off ');
		listaTablets += " </span><i class='fas fa-tablet-alt'></i></button>";
	});
	var listaMotos = '';
	let clase2 = '';
	mots.forEach((element) => {
		listaMotos +=
			"<button class='lista btn-list btn btn-light btn btn-block " +
			' align-items-center' +
			clase2 +
			"' onclick='ZoomMoto(this)' id='" +
			element.name +
			"'> ";
		element.status == 'online'
			? (listaMotos += "<span class='online'>" + element.name + ' On ')
			: (listaMotos += "<span class='offline'>" + element.name + ' Off ');
		listaMotos += " </span><i class='fas fa-motorcycle'></i></button>";
	});

	dispositivosListado += listaSideBar + listaTablets + listaMotos;
	L.innerHTML = dispositivosListado;
};

const lectores = (data) => {
	console.log('Lectores;', data);

	let lista = data.listado;
	let activos = data.activos;
	let lectoresActivos = [];
	let lectoresNoActivos = [];
	let bandera;

	for (let i = 0; i < lista.length; i++) {
		bandera = false;
		for (let j = 0; j < activos.length; j++) {
			if (activos[j].FCU == lista[i].unidad_lectorP) {
				lectoresActivos.push(lista[i]);
				bandera = true;
			}
		}
		if (bandera == false) {
			lectoresNoActivos.push(lista[i]);
		}
	}

	let lectores = document.getElementById('lectores');

	var lista1 = '';
	lectoresNoActivos.forEach((element) => {
		let cve = 'M-' + element.unidad_lectorP.slice(1) + '';
		lista1 +=
			"<button class='lista btn-list btn btn-light btn btn-block align-items-center " +
			"' onclick='Zoom(this)' id='" +
			cve +
			"'>" +
			"<span class='offline'> " +
			element.unidad_lectorP +
			" </span><i class='fas fa-car'></i></button>";
	});

	var lista2 = '';
	lectoresActivos.forEach((element) => {
		let cve = 'M-' + element.unidad_lectorP.slice(1) + '';
		lista2 +=
			"<button class='lista btn-list btn btn-light btn btn-block align-items-center " +
			"' onclick='Zoom(this)' id='" +
			cve +
			"'>" +
			"<span class='online'> " +
			element.unidad_lectorP +
			" Activo </span><i class='fas fa-car'></i></button>";
	});

	lectores.innerHTML = lista1 + lista2;
};

// Camaras
function Camaras(camaraMarkers) {
	layerCamara = new L.markerClusterGroup();
	for (var camara in camaraMarkers.camaras) {
		let { CamaraDescripcion, CamaraId, CamaraLat, CamaraLong, CamaraNo } = camaraMarkers.camaras[camara];
		if (CamaraLat != null && CamaraLong != null) {
			let imCamara = L.divIcon({
				className: 'custom-div-icon',
				html: "<div class='marker-pin'>" + CamaraNo + "</div><img src='/images/camara.png' class='img-fluid'/>",
			});
			layerCamara.addLayer(
				L.marker([CamaraLat, CamaraLong], { icon: imCamara }).bindPopup(
					"<a href='/video/" + CamaraId + "' target='_blank' class='camara btn'>" + CamaraDescripcion + '</a>'
				)
			);
		}
	}
}

// Botones
function Botones(botones) {
	layerBotones = new L.markerClusterGroup();
	if (botones.length > 0) {
		for (var boton in botones) {
			let { id, nombre, latitud, longitud } = botones[boton];
			if (latitud != '' && longitud != '') {
				layerBotones.addLayer(
					L.marker([latitud, longitud]).bindPopup("<a href='/videoB/" + id + "' target='_blank' class='camara btn'>" + nombre + '</a>')
				);
			}
		}
	}
}

//  Centros de salud
function CentroSalud(centros) {
	layerCentro = new L.layerGroup();
	for (var centro in centros) {
		let { municipio, nombre, Latitud, Longitud } = centros[centro];
		let _icon = L.divIcon({
			className: 'custom-div-icon',
			html: "<div class='marker-pin'>" + nombre + "</div><img src='/images/hospital.png' class='img-fluid'/>",
		});
		layerCentro.addLayer(L.marker([Latitud, Longitud], { icon: _icon }));
	}
}

// Luminarias
function Luminarias(luminarias) {
	//console.log('l', luminarias)
	let _iconL = L.icon({ iconUrl: '/images/luz-de-la-calle.png' });
	layerCH = new L.markerClusterGroup();
	for (var ch in luminarias.ch) {
		let { Delegacion, Latitud, Longitud } = luminarias.ch[ch];
		layerCH.addLayer(L.marker([Latitud, Longitud], { icon: _iconL }));
	}
	layerEG = new L.markerClusterGroup();
	for (var eg in luminarias.eg) {
		let { Delegacion, Latitud, Longitud } = luminarias.eg[eg];
		layerCH.addLayer(L.marker([Latitud, Longitud], { icon: _iconL }));
	}
	layerFCP = new L.markerClusterGroup();
	for (var fcp in luminarias.fcp) {
		let { Delegacion, Latitud, Longitud } = luminarias.fcp[fcp];
		layerCH.addLayer(L.marker([Latitud, Longitud], { icon: _iconL }));
	}
	layerFO = new L.markerClusterGroup();
	for (var fo in luminarias.fo) {
		let { Delegacion, Latitud, Longitud } = luminarias.fo[fo];
		layerCH.addLayer(L.marker([Latitud, Longitud], { icon: _iconL }));
	}
	layerJVH = new L.markerClusterGroup();
	for (var jvh in luminarias.jvh) {
		let { Delegacion, Latitud, Longitud } = luminarias.jvh[jvh];
		layerCH.addLayer(L.marker([Latitud, Longitud], { icon: _iconL }));
	}
	layerSRJ = new L.markerClusterGroup();
	for (var srj in luminarias.srj) {
		let { Delegacion, Latitud, Longitud } = luminarias.srj[srj];
		layerCH.addLayer(L.marker([Latitud, Longitud], { icon: _iconL }));
	}
	layerVCR = new L.markerClusterGroup();
	for (var vcr in luminarias.vcr) {
		let { Delegacion, Latitud, Longitud } = luminarias.vcr[vcr];
		layerCH.addLayer(L.marker([Latitud, Longitud], { icon: _iconL }));
	}
	layerVi = new L.markerClusterGroup();
	for (var vialidades in luminarias.vialidades) {
		let { Delegacion, Latitud, Longitud } = luminarias.vialidades[vialidades];
		layerCH.addLayer(L.marker([Latitud, Longitud], { icon: _iconL }));
	}
}

// Tabletas
let mostrarTabletasInicio = true;
function Tabletas(tabletaMarkers) {
	if (mostrarTabletasInicio) {
		mymap.addLayer(layerT);
		mostrarTabletasInicio = false;
	}
	layerT.clearLayers();
	if (marcadores.tabletas != null) {
		let mark;
		tabM = new Array();
		for (var tablet in tabletaMarkers) {
			let { latitud, longitud, name, lastupdate, CveGpoOPer, status } = tabletaMarkers[tablet];
			if (latitud != null && longitud != null && status == 'online') {
				let imTablet = L.divIcon({
					className: 'custom-div-icon',
					html: "<div class='marker-pin'>" + name + "</div><img src='/images/tablet-24.png' class='img-fluid'/>",
					iconSize: [45, 55],
					iconAnchor: [4, 5],
				});
				mark = L.marker([latitud, longitud], {
					icon: imTablet,
				}).bindPopup(
					"<p class='camara'>Nombre:" +
						name +
						'</p>' +
						"<p class='camara'>Region:" +
						CveGpoOPer +
						'</p>' +
						"<p class='camara'>Ulitmo registro:" +
						lastupdate +
						'</p>' +
						"<p class='camara'>Latitud y Longitud:" +
						latitud +
						' ,' +
						longitud +
						'</p>'
					// "Nombre: " + name + "; Fecha: " + lastupdate
				);
				layerT.addLayer(mark);
				tabM.push({
					mark: mark,
					name: name,
				});
			}
		}
	}
}

// Motocicletas
let mostrarMotocicletasInicio = true;
function Motocicletas(motocicletasMarkers) {
	if (mostrarMotocicletasInicio) {
		mymap.addLayer(layerM);
		mostrarMotocicletasInicio = false;
	}
	layerM.clearLayers();
	if (marcadores.motocicletas != null) {
		let mark;
		tabMot = new Array();
		for (var moto in motocicletasMarkers) {
			let { latitud, longitud, name, lastupdate, CveGpoOPer, status } = motocicletasMarkers[moto];
			if (latitud != null && longitud != null && status == 'online') {
				let imMoto = L.divIcon({
					className: 'custom-div-icon',
					html: "<div class='marker-pin'>" + name + "</div><img src='/images/moto2-32.png'/>",
					iconSize: [45, 55],
					iconAnchor: [4, 5],
				});
				mark = L.marker([latitud, longitud], {
					icon: imMoto,
				}).bindPopup(
					"<p class='camara'>Nombre:" +
						name +
						'</p>' +
						"<p class='camara'>Region:" +
						CveGpoOPer +
						'</p>' +
						"<p class='camara'>Ulitmo registro:" +
						lastupdate +
						'</p>' +
						"<p class='camara'>Latitud y Longitud:" +
						latitud +
						' ,' +
						longitud +
						'</p>'
					// "Nombre: " + name + "; Fecha: " + lastupdate
				);
				layerM.addLayer(mark);
				tabMot.push({
					mark: mark,
					name: name,
				});
			}
		}
	}
}

var patrullasEstado;
function gpsIcons(gpsEstado) {
	// console.log('GPS', gps)
	if (layerG != null) {
		layerG.clearLayers();
	}
	layerG = new L.layerGroup();
	markersG = new Array();
	let markG;
	if (marcadores.gps != null) {
		for (var gps in gpsEstado) {
			let { flota, latitud, longitud, rfsi, unidad } = gpsEstado[gps];
			if (latitud != null && longitud != null && flota != 'MOVILIDAD') {
				console.log(flota);
				if (unidad.search('PP-') != -1) {
					markG = L.marker([latitud, longitud], {
						icon: iconRadio,
					}).bindPopup('Unidad: ' + unidad + ' Flota: ' + flota + ' rfsi: ' + rfsi + ' Latitud y Longitud: ' + latitud + ' ,' + longitud);
				} else {
					markG = L.marker([latitud, longitud], {
						icon: iconGps,
					}).bindPopup('Unidad: ' + unidad + ' Flota: ' + flota + ' rfsi: ' + rfsi + ' Latitud y Longitud: ' + latitud + ' ,' + longitud);
				}
				layerG.addLayer(markG);
				markersG.push({
					markG: markG,
					unidad: unidad,
				});
			}
		}
	}
	mymap.addLayer(layerG);
}

function groupBy(list, keyGetter) {
	const map = new Map();
	list.forEach((item) => {
		const key = keyGetter(item);
		const collection = map.get(key);
		if (!collection) {
			map.set(key, [item]);
		} else {
			collection.push(item);
		}
	});
	return map;
}

function countAlerts() {
	try {
		if (alertas.length > 0) {
			for (let i = 0; alertas.length > i; i++) {
				let marcador = patrullas.filter((ele) => ele.unidad == alertas[i].CveVehi);
				let ic = L.divIcon({
					className: 'custom-div-icon',
					html: "<div class='marker-pin'>" + alertas[i].CveVehi + "</div><img src='/images/AlertIcon.ico' class='img-fluid'/>",
					iconSize: [50, 62],
					iconAnchor: [8, 16],
				});
				marcador[0].marcador.setIcon(ic);
			}
			const Toast = Swal.mixin({
				toast: true,
				position: 'top',
				showConfirmButton: true,
				timer: 15000,
				timerProgressBar: true,
				showCancelButton: true,
				width: '20em',
			});
			Toast.fire({
				icon: 'warning',
				title: 'Hay ' + alertas.length + ' alertas',
				confirmButtonText: 'Ver',
				cancelButtonText: '&times;',
			}).then((result) => {
				if (result.isConfirmed) {
					window.open('alertas');
				}
			});
		}
	} catch (error) {
		console.log('Error', error);
	}
}

function insertAlerts(alerts) {
	localStorage.setItem('alertas', JSON.stringify(alertas));
}

function Zoom(item) {
	var id = '';
	var mark = null;
	var mark2 = null;
	id = item.id;
	var iconCarO = L.icon({
		iconUrl: '/images/icono2.png',
		iconSize: [40, 40],
		iconAnchor: [8, 16],
	});
	var markersG = new Array();
	if (markersG.length > 0) {
		mark2 = markersG.filter((ele) => ele.unidad == id);
		if (mark2[0] != null) {
			$('#modal-lista').modal('hide');
			mark2[0].markG.setIcon(iconCarO).openPopup();
			mymap.setView(mark2[0].markG.getLatLng(), 20);
			$('#navbarsExampleDefault').collapse({
				hide: true,
			});
			console.log('No se encontró la unidad');
		} else {
			console.log('No se encontró la unidad');
			Toasty.fire({
				icon: 'error',
				title: '¡No se encontró la unidad!',
				background: '#FFD6DA',
			});
			$('#navbarsExampleDefault').collapse({
				hide: true,
			});
		}
	}
	if (patrullas.length > 0) {
		mark = patrullas.filter((ele) => ele.unidad == id);
		if (mark[0] != null) {
			if (calculardiferencia(mark[0].marcador._popup._content.substr(-20, 20))) {
				mark[0].marcador.setIcon(iconCarFail).openPopup();
			} else {
				mark[0].marcador.setIcon(iconCarO).openPopup();
			}
			$('#modal-lista').modal('hide');
			$('#modal-lectores').modal('hide');
			mymap.setView(mark[0].marcador.getLatLng(), 20);
			$('#navbarsExampleDefault').collapse('hide');
		} else {
			Toasty.fire({
				icon: 'error',
				title: '¡No se encontró la unidad!',
				background: '#FFD6DA',
			});
			$('#navbarsExampleDefault').collapse('hide');
		}
	}
}

function ZoomTablet(item) {
	//alert(JSON.stringify(item))
	var id = '';
	var marka = null;
	id = item.id;
	let findTablet = L.icon({
		iconUrl: '/images/findTablet.png',
		iconSize: [40, 40],
		iconAnchor: [8, 16],
	});
	marka = tabM.filter((ele) => ele.name == id);
	if (marka[0] != null) {
		$('#modal-lista').modal('hide');
		marka[0].mark.setIcon(findTablet).openPopup();
		mymap.setView(marka[0].mark.getLatLng(), 20);
	} else {
		Toasty.fire({
			icon: 'error',
			title: '¡No se encontró la tableta!',
			background: '#FFD6DA',
		});
	}
}

function ZoomMoto(item) {
	//alert(JSON.stringify(item))
	var idm = '';
	var markam = null;
	idm = item.id;
	let findMoto = L.icon({
		iconUrl: '/images/moto2-32.png',
		iconSize: [40, 40],
		iconAnchor: [8, 16],
	});
	markam = tabMot.filter((ele) => ele.name == idm);
	if (markam[0] != null) {
		$('#modal-lista').modal('hide');
		markam[0].mark.setIcon(findMoto).openPopup();
		mymap.setView(markam[0].mark.getLatLng(), 20);
	} else {
		Toasty.fire({
			icon: 'error',
			title: '¡No se encontró la tableta!',
			background: '#FFD6DA',
		});
	}
}

function Redirect(url) {
	let permisos = document.getElementById('agregarPersonal');
	if (permisos != null) {
		var u = url.name;
		window.location.href = u;
	}
}

//Se obtienen la lista de claves de empleados
async function obtenerInfoUnidad(boton) {
	let data = null;
	let listaPersonal = '';
	let listaEquipo = '';
	//se obtienen las personas que estan en la unidad
	let clavesPersonal = await fetch('/InfoUnidad', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
		},
		body: JSON.stringify({ CveVehi: boton.id }),
	});
	data = await clavesPersonal.json();
	$('#tituloInfo').text('Unidad:' + data.unidad[0].CveVehi + '| Placa:' + data.unidad[0].PlacaVehi + '| Economico:' + data.unidad[0].NumEcoVehi);
	for (var x = 0; x < data.equipo.length; x++) {
		listaEquipo += "<li class='list-group-item'>" + data.equipo[x].EquipoDesc + ' ' + 'Cantidad: ' + data.equipo[x].EquipoCantidad + '</li>';
	}
	for (var i = 0; i < data.personal.length; i++) {
		listaPersonal += "<li class='list-group-item'> Empleado: " + data.personal[i].CveNumEmp + ' ' + data.personal[i].nombre + '</li>';
	}
	if (data.personal[0] != null) {
		$('#ListaUnidad').html("<li class='list-group-item'>" + boton.id + '</li>');
		$('#ListaPersonal').html(listaPersonal);
	} else {
		$('#ListaPersonal').html('<h5>No hay informacion que mostrar</h5>');
	}
	if (data.equipo[0] != null) {
		$('#ListaEquipo').html(listaEquipo);
	} else {
		$('#ListaEquipo').html('<h5><h5/>');
	}
}

const Toasty = Swal.mixin({
	toast: true,
	position: 'top-start',
	showConfirmButton: false,
	timer: 3000,
	timerProgressBar: true,
});
