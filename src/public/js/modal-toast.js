function abrirModal(tipo, mensaje) {
  let esqueletoModal = esqueletoModal(tipo, mensaje);
  let modal = document.getElementById("myModal");
  modal.innerHTML = esqueletoModal;
  $("#myModal").modal("show");
}
function esqueletoModal(tipo, mensaje) {
  let cajaModal = "";
  let cabezeraModal = "";
  let titulo = "";
  let btnCerrarModal = "";
  let botones = "";

  if (tipo == "info") {
    cajaModal = "cajaModalInfo";
    cabezeraModal = "cabezeraModalInfo";
    titulo = " ℹ SISCOP ";
    btnCerrarModal = "btnCerrarModalBlanco";
    botones =
      " <button" +
      ' type="button"' +
      '     class="btn btn-amarillo"' +
      '    data-dismiss="modal"' +
      "   >Cerrar" +
      "   </button>";
  }
  if (tipo == "pregunta") {
    cajaModal = "cajaModalPregunta";
    cabezeraModal = "cabezeraModalPregunta";
    titulo = " ❔ SISCOP ";
    btnCerrarModal = "btnCerrarModalBlanco";
    botones =
      " <button" +
      ' type="button"' +
      '     class="btn btn-azul"' +
      '    data-dismiss="modal"' +
      ' onclick="acepta()"' +
      "   >Aceptar" +
      "   </button> ";
    botones +=
      " <button" +
      ' type="button"' +
      '     class="btn btn-amarillo"' +
      '    data-dismiss="modal"' +
      "   >Cerrar" +
      "   </button> ";
  }
  if (tipo == "error") {
    cajaModal = "cajaModalError";
    cabezeraModal = "cabezeraModalError";
    titulo = " ⚠ SISCOP ";
    btnCerrarModal = "btnCerrarModalNegro";
    botones =
      " <button" +
      ' type="button"' +
      '     class="btn btn-amarillo"' +
      '    data-dismiss="modal"' +
      "   >Cerrar" +
      "   </button>";
  }
  if (tipo == "bien") {
    cajaModal = "cajaModalBien";
    cabezeraModal = "cabezeraModalBien";
    titulo = " ✅ SISCOP ";
    btnCerrarModal = "btnCerrarModalNegro";
    botones =
      " <button" +
      ' type="button"' +
      '     class="btn btn-azul"' +
      '    data-dismiss="modal"' +
      "   >Cerrar" +
      "   </button>";
  }
  return (
    '<div class="modal-dialog">' +
    '<div class="modal-content" id="' +
    cajaModal +
    '">' +
    //Modal Header
    '<div class="modal-header" id="' +
    cabezeraModal +
    '">' +
    '<h4 class="modal-title">' +
    titulo +
    "</h4>" +
    " <button" +
    ' type="button"' +
    ' class="close"' +
    '   data-dismiss="modal"' +
    '  id="' +
    btnCerrarModal +
    '"' +
    "  >" +
    " &times;" +
    " </button>" +
    " </div>" +
    //ModalBody
    '<div class="modal-body">' +
    mensaje +
    " </div>" +
    //Modal footer
    '<div class="modal-footer">' +
    botones +
    " </div>" +
    "</div>" +
    "</div>"
  );
}
