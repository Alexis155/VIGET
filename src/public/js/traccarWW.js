/*self.onmessage = function (e) {
  console.log("Message received from main script");
  
  fetch("/obtenerInfoTraccar", {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ cveVehi }),
  })
    .then((res) => {
      res.json().then((data) => {
        postMessage(data);
      });
    })
    .catch((err) => {
      console.log(err);
    });
};*/

onmessage = function (e) {
  console.log("Message received from main script");
  obtenerInfoTraccar(e.data);
};
let obtenerInfoTraccar = (url) => {
  fetch("/obtenerInfoTraccar", {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
    },
  })
    .then((res) => {
      res.json().then((data) => {
        postMessage(data);
        console.log("Posting message back to main script");
      });
    })
    .catch((err) => {
      console.log(err);
    });
  setTimeout("obtenerInfoTraccar()", 30000);
};
