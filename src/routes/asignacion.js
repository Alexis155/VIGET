//Imports
const express = require("express");
const router = express.Router();
const AsignacionController = require("../controllers/AsignacionController");
const { isLoggedIn } = require("../middlewares/login");

router.get("/asignacion/:CveVehi/:CveGpoOper", isLoggedIn, AsignacionController.index);
router.post("/agregar",isLoggedIn,AsignacionController.Insert);
router.post("/obtenerAsignaciones", isLoggedIn, AsignacionController.obtenerAsignaciones);
router.post("/desasginarOficial", isLoggedIn, AsignacionController.desasginarOficial);
router.post("/reAsginarOficial", isLoggedIn, AsignacionController.reAsginarOficial);
module.exports = router;
