//Imports
const express = require("express");
const router = express.Router();
const { isLoggedIn } = require("../middlewares/login");
const camraController = require("../controllers/CamaraController");

router.get("/video/:idCam", isLoggedIn, camraController.index);
router.get("/videoB/:idCam", isLoggedIn, camraController.botones);

//exports
module.exports = router;
