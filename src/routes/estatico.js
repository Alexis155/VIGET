const express = require("express");
const router = express.Router();
const estaticoController = require("../controllers/EstaticoController");
const { isLoggedIn } = require("../middlewares/login");
/*
router.get('/estatico', isLoggedIn, estaticoController.index);*/
router.get('/estatico', isLoggedIn, estaticoController.index);
router.post('/estaticoU', isLoggedIn, estaticoController.consulta);
module.exports = router;