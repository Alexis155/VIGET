const express = require('express');
const router = express.Router();
const historicoController = require('../controllers/HistoricoController');
const { isLoggedIn } = require('../middlewares/login');

router.get('/historico', isLoggedIn, historicoController.index);
router.post('/historicoU', isLoggedIn, historicoController.consulta);
router.post('/historicoTraccar', isLoggedIn, historicoController.consultaTraccar);
router.post('/historicoAll', isLoggedIn, historicoController.consultaAll);
module.exports = router;
