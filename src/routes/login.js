//Imports
const express = require("express");
const passport = require("passport");
const router = express.Router();
const { isNotLoggedIn } = require("../middlewares/login");
//Métodos
router.get("/", isNotLoggedIn, (req, res) => {
  res.render("login.html");
});

// poner ruta para que reciba parametros de usuraio y contraseña
router.get("/login", isNotLoggedIn, (req, res) => {
  res.render("login.html");
});

router.post("/login", (req, res, next) => {
  passport.authenticate("local.login", {
    successRedirect: "/mapa",
    failureRedirect: "/",
    failureFlash: true,
  })(req, res, next);
});

router.get("/logout", (req, res) => {
  req.logOut();
  res.redirect("/");
});
//exports
module.exports = router;
