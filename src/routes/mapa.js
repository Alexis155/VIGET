const express = require('express');
const router = express.Router();
const mapaController = require('../controllers/MapaController');
const alertasController = require('../controllers/AlertasController');
const { isLoggedIn } = require('../middlewares/login');

router.get('/mapa', isLoggedIn, mapaController.index);
router.get('/mapa/:lat/:long', isLoggedIn, mapaController.indexP);
router.post('/obtenerCapasEstaticas', isLoggedIn, mapaController.camaras);
router.get('/botones', isLoggedIn, mapaController.botones);
router.post('/obtenerInfoTraccar', isLoggedIn, mapaController.traccar);
router.post('/obtenerInfoVehiculo', isLoggedIn, mapaController.vehiculos);
router.post('/unidades', isLoggedIn, mapaController.listaVehiculos);
router.post('/regiones', isLoggedIn, mapaController.regiones);
router.post('/lectores', isLoggedIn, mapaController.lectores);
router.post('/InfoUnidad', isLoggedIn, mapaController.infoUnidad);
router.get('/limites', isLoggedIn, mapaController.limitesRegiones);
router.post('/insertAlert', isLoggedIn, mapaController.insertAlert);
router.get('/gpsEstado', isLoggedIn, mapaController.gpsEstado);
//alertas
router.get('/alertas', isLoggedIn, alertasController.index);
router.get('/count', isLoggedIn, alertasController.count);
// emergencias
router.get('/emergencias', isLoggedIn, mapaController.emergencias);

router.get('/reporte', isLoggedIn, mapaController.reporteGPS);

router.get('/centros', isLoggedIn, mapaController.centrosSalud);
router.get('/luminarias', isLoggedIn, mapaController.luminarias);

//exports
module.exports = router;
