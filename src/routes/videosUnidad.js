const express = require("express");
const router = express.Router();
const { isLoggedIn } = require("../middlewares/login");
const VideoController = require("../controllers/VideoController");

router.get("/videos/:idUnidad/:unidad", isLoggedIn,VideoController.index);
router.post("/insert", isLoggedIn, VideoController.insert);
router.post("/update", isLoggedIn, VideoController.update);
router.post("/video/url", isLoggedIn, VideoController.ObnterVideo);
router.post("/bloqueo", isLoggedIn, VideoController.bloqueo);
module.exports = router;